# DREME 4.12.0
#     command: dreme -v 1 -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_not-in-DHSs-noAHREs.bed.fa/jaspar/seqs-centered -n results/AHRpeaks_not-in-DHSs-noAHREs.bed.fa/jaspar/seqs-shuffled -e 0.05
#   positives: 38 from results/AHRpeaks_not-in-DHSs-noAHREs.bed.fa/jaspar/seqs-centered (Thu May 24 14:17:54 EDT 2018)
#   negatives: 38 from results/AHRpeaks_not-in-DHSs-noAHREs.bed.fa/jaspar/seqs-shuffled (Thu May 24 14:17:54 EDT 2018)
#        host: Sudins-iMac.local
#        when: Thu May 24 14:18:02 EDT 2018

MEME version 4.12.0

ALPHABET "DNA" DNA-LIKE
A "Adenine" CC0000 ~ T "Thymine" 008000
C "Cytosine" 0000CC ~ G "Guanine" FFB300
N "Any base" = ACGT
X = ACGT
. = ACGT
V "Not T" = ACG
H "Not G" = ACT
D "Not C" = AGT
B "Not A" = CGT
M "Amino" = AC
R "Purine" = AG
W "Weak" = AT
S "Strong" = CG
Y "Pyrimidine" = CT
K "Keto" = GT
U = T
END ALPHABET

strands: + -

Background letter frequencies (from dataset):
A 0.269 C 0.195 G 0.254 T 0.282


# Stopping reason: E-value threshold exceeded
#    Running time: 0.24 seconds
