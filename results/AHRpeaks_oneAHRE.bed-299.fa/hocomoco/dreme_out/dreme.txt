# DREME 4.12.0
#     command: dreme -v 1 -oc results/AHRpeaks_oneAHRE.bed-299.fa/hocomoco/dreme_out -png -dna -p results/AHRpeaks_oneAHRE.bed-299.fa/hocomoco/seqs-centered -n results/AHRpeaks_oneAHRE.bed-299.fa/hocomoco/seqs-shuffled -e 0.05
#   positives: 843 from results/AHRpeaks_oneAHRE.bed-299.fa/hocomoco/seqs-centered (Thu May 24 15:17:51 EDT 2018)
#   negatives: 843 from results/AHRpeaks_oneAHRE.bed-299.fa/hocomoco/seqs-shuffled (Thu May 24 15:17:51 EDT 2018)
#        host: Sudins-iMac.local
#        when: Thu May 24 15:31:33 EDT 2018

MEME version 4.12.0

ALPHABET "DNA" DNA-LIKE
A "Adenine" CC0000 ~ T "Thymine" 008000
C "Cytosine" 0000CC ~ G "Guanine" FFB300
N "Any base" = ACGT
X = ACGT
. = ACGT
V "Not T" = ACG
H "Not G" = ACT
D "Not C" = AGT
B "Not A" = CGT
M "Amino" = AC
R "Purine" = AG
W "Weak" = AT
S "Strong" = CG
Y "Pyrimidine" = CT
K "Keto" = GT
U = T
END ALPHABET

strands: + -

Background letter frequencies (from dataset):
A 0.272 C 0.223 G 0.233 T 0.272


MOTIF DCACGC DREME-1

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST      DCACGC     GCGTGH        190         37   9.3e-030   3.2e-025
#           GCACGC     GCGTGC         80         13   7.7e-014   2.6e-009
#           TCACGC     GCGTGA         60         10   1.9e-010   6.3e-006
#           ACACGC     GCGTGT         50         16   1.2e-005   4.0e-001

letter-probability matrix: alength= 4 w= 6 nsites= 190 E= 3.2e-025
0.263158 0.000000 0.421053 0.315789
0.000000 1.000000 0.000000 0.000000
1.000000 0.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.000000 0.000000 1.000000 0.000000
0.000000 1.000000 0.000000 0.000000


MOTIF TRTTTR DREME-2

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST      TRTTTR     YAAAYA        344        191   6.5e-016   2.2e-011
#           TATTTG     CAAATA        100         44   6.3e-007   2.1e-002
#           TGTTTA     TAAACA        115         63   2.4e-005   8.0e-001
#           TATTTA     TAAATA         91         47   6.0e-005   2.0e+000
#           TGTTTG     CAAACA        118         69   9.2e-005   3.1e+000

letter-probability matrix: alength= 4 w= 6 nsites= 446 E= 2.2e-011
0.000000 0.000000 0.000000 1.000000
0.441704 0.000000 0.558296 0.000000
0.000000 0.000000 0.000000 1.000000
0.000000 0.000000 0.000000 1.000000
0.000000 0.000000 0.000000 1.000000
0.475336 0.000000 0.524664 0.000000


MOTIF TGACTCA DREME-3

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST     TGACTCA    TGAGTCA         63         11   1.2e-010   3.8e-006
#          TGACTCA    TGAGTCA         63         11   1.2e-010   3.8e-006

letter-probability matrix: alength= 4 w= 7 nsites= 66 E= 3.8e-006
0.000000 0.000000 0.000000 1.000000
0.000000 0.000000 1.000000 0.000000
1.000000 0.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.000000 0.000000 0.000000 1.000000
0.000000 1.000000 0.000000 0.000000
1.000000 0.000000 0.000000 0.000000


MOTIF CACMCAC DREME-4

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST     CACMCAC    GTGKGTG         68         16   1.9e-009   6.1e-005
#          CACACAC    GTGTGTG         43          9   7.2e-007   2.4e-002
#          CACCCAC    GTGGGTG         26          7   5.9e-004   1.9e+001

letter-probability matrix: alength= 4 w= 7 nsites= 75 E= 6.1e-005
0.000000 1.000000 0.000000 0.000000
1.000000 0.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.653333 0.346667 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
1.000000 0.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000


MOTIF ATCTYTC DREME-5

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST     ATCTYTC    GARAGAT         60         14   1.6e-008   5.3e-004
#          ATCTCTC    GAGAGAT         33          5   1.7e-006   5.5e-002
#          ATCTTTC    GAAAGAT         28          9   1.2e-003   3.8e+001

letter-probability matrix: alength= 4 w= 7 nsites= 62 E= 5.3e-004
1.000000 0.000000 0.000000 0.000000
0.000000 0.000000 0.000000 1.000000
0.000000 1.000000 0.000000 0.000000
0.000000 0.000000 0.000000 1.000000
0.000000 0.548387 0.000000 0.451613
0.000000 0.000000 0.000000 1.000000
0.000000 1.000000 0.000000 0.000000


MOTIF TTATCW DREME-6

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST      TTATCW     WGATAA        148         77   2.2e-007   7.3e-003
#           TTATCT     AGATAA         90         42   8.5e-006   2.7e-001
#           TTATCA     TGATAA         66         37   2.1e-003   6.8e+001

letter-probability matrix: alength= 4 w= 6 nsites= 164 E= 7.3e-003
0.000000 0.000000 0.000000 1.000000
0.000000 0.000000 0.000000 1.000000
1.000000 0.000000 0.000000 0.000000
0.000000 0.000000 0.000000 1.000000
0.000000 1.000000 0.000000 0.000000
0.408537 0.000000 0.000000 0.591463


MOTIF CCGCCYC DREME-7

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST     CCGCCYC    GRGGCGG         20          0   8.5e-007   2.7e-002
#          CCGCCTC    GAGGCGG         13          0   1.2e-004   3.8e+000
#          CCGCCCC    GGGGCGG          7          0   7.7e-003   2.5e+002

letter-probability matrix: alength= 4 w= 7 nsites= 20 E= 2.7e-002
0.000000 1.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.000000 0.000000 1.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.000000 1.000000 0.000000 0.000000
0.000000 0.350000 0.000000 0.650000
0.000000 1.000000 0.000000 0.000000


# Stopping reason: E-value threshold exceeded
#    Running time: 33.58 seconds
