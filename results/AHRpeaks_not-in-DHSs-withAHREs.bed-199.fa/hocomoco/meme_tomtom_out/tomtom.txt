#Query ID	Target ID	Optimal offset	p-value	E-value	q-value	Overlap	Query consensus	Target consensus	Orientation
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	ONEC2_HUMAN.H11MO.0.D	-4	0.000113119	0.0872148	0.0679567	21	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	GTCTTGTTATTGATTTTTTTT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	CDX2_HUMAN.H11MO.0.A	-8	0.000133047	0.102579	0.0679567	12	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTTTATTGCTGT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	ARI3A_HUMAN.H11MO.0.D	-3	0.000134864	0.10398	0.0679567	22	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTTAATTTGATTTCGATTAATT	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	FOXF1_HUMAN.H11MO.0.D	0	0.000281712	0.2172	0.0921666	11	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	ATGTTTATTTT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	STAT1_HUMAN.H11MO.1.A	-6	0.000304849	0.235039	0.0921666	19	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	CTTTCAGTTTCATTTTCTT	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	CPEB1_HUMAN.H11MO.0.D	-7	0.000403583	0.311163	0.101681	17	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTTTTATTTTTTTTTTT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	IRF1_HUMAN.H11MO.0.A	-3	0.000709421	0.546963	0.116015	20	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTACTTTCACTTTCATTTTC	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	SRY_HUMAN.H11MO.0.B	-2	0.000772694	0.595747	0.116015	9	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTTTGTTTT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	EVX2_HUMAN.H11MO.0.A	-7	0.000836621	0.645035	0.116015	13	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	GTTTTATGGCCTT	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	FOXD3_HUMAN.H11MO.0.D	-3	0.000840349	0.647909	0.116015	15	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTTGTTTACTTAGCA	+
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	STAT2_HUMAN.H11MO.0.A	-6	0.000876554	0.675823	0.116015	19	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	CTTTCAGTTTCATTTTCCT	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	PO2F1_HUMAN.H11MO.0.C	-9	0.001004	0.774084	0.116015	16	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TTATTTGCATTCGATT	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	ZN394_HUMAN.H11MO.0.C	-2	0.00102393	0.789453	0.116015	20	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	ATTCCATTCTATTCCATTCT	-
ATCTTTGTTTTTATTKCATTTGCTTTTGKG	FOXJ2_HUMAN.H11MO.0.C	-1	0.00107445	0.828399	0.116015	10	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TGTTTATTTA	+
GMWYRRSAAAVGKAASAGKVAG	ZN816_HUMAN.H11MO.1.C	-7	9.32921e-05	0.0719282	0.11271	14	GCATGGCAAAAGGAACAGGCAG	AAGGGGGACATGCA	+
GMWYRRSAAAVGKAASAGKVAG	GCR_HUMAN.H11MO.1.A	-9	0.000162122	0.124996	0.11271	12	GCATGGCAAAAGGAACAGGCAG	AAAGAACAGAAT	+
GMWYRRSAAAVGKAASAGKVAG	ANDR_HUMAN.H11MO.2.A	-8	0.000227495	0.175399	0.11271	12	GCATGGCAAAAGGAACAGGCAG	AAAAGAACAGAG	-
GMWYRRSAAAVGKAASAGKVAG	IRF3_HUMAN.H11MO.0.B	0	0.000484291	0.373389	0.172048	20	GCATGGCAAAAGGAACAGGCAG	GAAAGGGAAAGGGAAAGTGA	+
GMWYRRSAAAVGKAASAGKVAG	ZN263_HUMAN.H11MO.0.A	-4	0.000857884	0.661429	0.172048	18	GCATGGCAAAAGGAACAGGCAG	GGGAGGAGGGAGAGGAGGAG	+
GMWYRRSAAAVGKAASAGKVAG	SPI1_HUMAN.H11MO.0.A	-4	0.00100944	0.778276	0.172048	17	GCATGGCAAAAGGAACAGGCAG	AAAAAGAGGAAGTGAAA	+
GMWYRRSAAAVGKAASAGKVAG	COT2_HUMAN.H11MO.0.A	-6	0.0010923	0.842166	0.172048	13	GCATGGCAAAAGGAACAGGCAG	CAAAGGTCAGAGG	+
GMWYRRSAAAVGKAASAGKVAG	BC11A_HUMAN.H11MO.0.A	-5	0.00120665	0.930325	0.172048	17	GCATGGCAAAAGGAACAGGCAG	AAAAGAGGAAGTGAAAA	+
GMWYRRSAAAVGKAASAGKVAG	ZN816_HUMAN.H11MO.0.C	-3	0.00127724	0.984753	0.172048	19	GCATGGCAAAAGGAACAGGCAG	AAAAAAGGGGGACATGCAGGG	+
GMWYRRSAAAVGKAASAGKVAG	SPIB_HUMAN.H11MO.0.A	-4	0.00127999	0.986875	0.172048	17	GCATGGCAAAAGGAACAGGCAG	AAAAAGAGGAAGTGAAA	+
GMWYRRSAAAVGKAASAGKVAG	IRF4_HUMAN.H11MO.0.A	-5	0.00128323	0.989374	0.172048	17	GCATGGCAAAAGGAACAGGCAG	AAAAGAGGAAGTGAAACT	+
AADSBCHTHAGTYTWVTTWCKTMCCAGMWA	HXD9_HUMAN.H11MO.0.D	-9	0.000666313	0.513727	0.359485	10	AATGCCTTTAGTTTAATTACTTCCCAGCAA	AATTTAATTA	+
AADSBCHTHAGTYTWVTTWCKTMCCAGMWA	ARX_HUMAN.H11MO.0.D	-7	0.000824034	0.63533	0.359485	13	AATGCCTTTAGTTTAATTACTTCCCAGCAA	TTAATTTAATTAG	+
AADSBCHTHAGTYTWVTTWCKTMCCAGMWA	ALX3_HUMAN.H11MO.0.D	-8	0.00101757	0.784547	0.359485	11	AATGCCTTTAGTTTAATTACTTCCCAGCAA	TAATTTAATTA	-
TTCHAKAAGATAACATYRGAAAAACYCTKC	EVI1_HUMAN.H11MO.0.B	-6	0.000501001	0.386272	0.568623	16	TTCTATAAGATAACATTAGAAAAACTCTTC	AAGATAAGATAAGATA	+
TTCHAKAAGATAACATYRGAAAAACYCTKC	ZN384_HUMAN.H11MO.0.C	-17	0.000917638	0.707499	0.568623	12	TTCTATAAGATAACATTAGAAAAACTCTTC	GGAAAAAATCGG	+
TTCHAKAAGATAACATYRGAAAAACYCTKC	HSF1_HUMAN.H11MO.0.A	0	0.00110627	0.852935	0.568623	15	TTCTATAAGATAACATTAGAAAAACTCTTC	TTCTAGAACCTTCCA	-
TTGSCYKAGGSRAGRMTTKCWTGMCYAA	AP2C_HUMAN.H11MO.0.A	1	0.000927759	0.715302	0.719642	13	TTGCCTTAGGCAAGGATTTCATGACCAA	ATGCCCTGAGGCCA	+
GGTKKBRGGTYT	GLI2_HUMAN.H11MO.0.D	-1	0.000672104	0.518192	0.906019	11	GGTTTGAGGTCT	GTGGGTGGTCT	+
GGTKKBRGGTYT	GLIS1_HUMAN.H11MO.0.D	1	0.00129116	0.995487	0.906019	12	GGTTTGAGGTCT	TCGTGGGGGGTCT	+
GGCAGAG	SMAD1_HUMAN.H11MO.0.D	0	0.00129641	0.999533	0.660367	7	GGCAGAG	GGCAGACAGGCT	-
