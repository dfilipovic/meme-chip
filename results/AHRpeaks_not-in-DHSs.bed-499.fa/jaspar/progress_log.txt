Invoking:
  getsize results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa 1> $metrics
Finished invoke:
  name: count_seqs  status: 0  time: 0.006778
Invoking:
  fasta-most -min 50 < results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa 1> $metrics
Finished invoke:
  name: most_seqs  status: 0  time: 0.035023
Invoking:
  fasta-center -dna -len 100 < results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa 1> results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-centered
Finished invoke:
  name: center_seqs  status: 0  time: 0.040564
Invoking:
  fasta-shuffle-letters results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-centered results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-shuffled -kmer 2 -tag -dinuc -dna -seed 1
Finished invoke:
  name: shuffle_seqs  status: 0  time: 0.00321
Invoking:
  fasta-get-markov -nostatus -nosummary -dna -m 1 results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background
Finished invoke:
  name: bg  status: 0  time: 0.003065
Invoking:
  meme results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-centered -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out -mod zoops -nmotifs 7 -minw 5 -maxw 30 -bfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -dna -revcomp -nostatus
Finished invoke:
  name: meme  status: 0  time: 8.577536
Invoking:
  dreme -v 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-centered -n results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/seqs-shuffled -e 0.05
Finished invoke:
  name: dreme  status: 0  time: 0.444104
Invoking:
  centrimo -seqlen 499 -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/centrimo_out -bfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -score 5.0 -ethresh 10.0 results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: centrimo  status: 0  time: 1.902116
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: meme_tomtom  status: 0  time: 29.879165
Invoking:
  tomtom -verbosity 1 -text -thresh 0.1 results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/combined.meme results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/combined.meme 1> results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/motif_alignment.txt
Finished invoke:
  name: align  status: 0  time: 0.249245
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/spamo_out_1 -bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -primary CAAAAGCAAATRCAATARAAACAAAGATAA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo1  status: 0  time: 1.790563
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/spamo_out_2 -bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -primary CTTGGTCATGAAATYCTTGCCTAAGSCAA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo2  status: 0  time: 1.914632
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/spamo_out_3 -bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -primary TAGTTTAATTAAGTCCCAGCTA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo3  status: 0  time: 1.802598
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/spamo_out_4 -bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background -primary YTTGCRTGRCAAARGKAASAG results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo4  status: 0  time: 1.816577
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/fimo_out_1 --bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background --motif CAAAAGCAAATRCAATARAAACAAAGATAA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa
Finished invoke:
  name: fimo1  status: 0  time: 0.051443
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/fimo_out_2 --bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background --motif CTTGGTCATGAAATYCTTGCCTAAGSCAA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa
Finished invoke:
  name: fimo2  status: 0  time: 0.042912
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/fimo_out_3 --bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background --motif TAGTTTAATTAAGTCCCAGCTA results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa
Finished invoke:
  name: fimo3  status: 0  time: 0.042065
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/fimo_out_4 --bgfile results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/background --motif YTTGCRTGRCAAARGKAASAG results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar/AHRpeaks_not-in-DHSs.bed-499.fa
Finished invoke:
  name: fimo4  status: 0  time: 0.042358
Writing output
Invoking:
  meme-chip_html_to_tsv ./results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar//meme-chip.html ./results/AHRpeaks_not-in-DHSs.bed-499.fa/jaspar//summary.tsv
Finished invoke:
  name: summary  status: 0  time: 0.100838
Done
