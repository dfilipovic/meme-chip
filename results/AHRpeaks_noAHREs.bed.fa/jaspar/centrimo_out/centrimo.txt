# WARNING: this file is not sorted!
# db	id                         alt	consensus	 E-value	adj_p-value	log_adj_p-value	bin_location	bin_width	total_width	sites_in_bin	total_sites	p_success	 p-value	mult_tests
   2	HCGTGM                 DREME-2	HCGTGM	4.0e-001	   2.8e-004	          -8.18	         0.0	      298	        544	          56	         68	  0.54779	1.0e-006	       271
   3	MA0016.1                   usp	GGGGTCACGS	3.2e0000	   2.2e-003	          -6.11	         0.0	       42	        540	          15	         53	  0.07778	8.3e-006	       269
   3	MA0047.2                 Foxa2	TGTTTACWYWGB	1.8e0000	   1.3e-003	          -6.65	         0.0	      268	        538	          89	        128	  0.49814	4.8e-006	       268
   3	MA0296.1                  FKH1	NNWWWTGTAAACAAARRNGR	8.8e0000	   6.2e-003	          -5.09	         0.0	      230	        530	          62	         96	  0.43396	2.3e-005	       264
   3	MA0546.1                 pha-4	WRWGYAAAYA	2.6e-001	   1.8e-004	          -8.60	         0.0	      312	        540	          92	        115	  0.57778	6.8e-007	       269
   3	MA0643.1                 Esrrg	TCAAGGTCAH	4.6e0000	   3.2e-003	          -5.73	         0.0	       50	        540	          23	         94	  0.09259	1.2e-005	       269
   3	MA0033.2                 FOXL1	RTAAACA	8.6e0000	   6.1e-003	          -5.11	         0.0	      273	        543	          92	        135	  0.50276	2.2e-005	       271
   3	MA0647.1                 GRHL1	AAAACCGGTTTD	7.5e-002	   5.3e-005	          -9.85	         0.0	      322	        538	          51	         56	  0.59851	2.0e-007	       268
   3	MA0481.2                 FOXP1	NDGTAAACAGNN	6.1e0000	   4.3e-003	          -5.45	         0.0	      290	        538	          95	        132	  0.53903	1.6e-005	       268
   3	MA0036.3                 GATA2	NBCTTATCTNH	9.0e0000	   6.4e-003	          -5.06	         0.0	      105	        539	          45	        128	  0.19481	2.4e-005	       269
##
# Detailed descriptions of columns in this file:
#
# db:	The name of the database (file name) that contains the motif.
# id:	A name for the motif that is unique in the motif database file.
# alt:	An alternate name of the motif that may be provided
#	in the motif database file.
# consensus:	A consensus sequence computed from the motif.
# E-value:	The expected number motifs that would have least one.
#	region as enriched for best matches to the motif as the reported region.
#	The E-value is the p-value multiplied by the number of motifs in the
#	input database(s).
# adj_p-value:	The probability that any tested region would be as enriched for
#	best matches to this motif as the reported region is.
#	By default the p-value is calculated by using the one-tailed binomial
#	test on the number of sequences with a match to the motif 
#	that have their best match in the reported region, corrected for
#	the number of regions and score thresholds tested.
#	The test assumes that the probability that the best match in a sequence
#	falls in the region is the region width divided by the
#	number of places a motif
#	can align in the sequence (sequence length minus motif width plus 1).
#	When CentriMo is run in discriminative mode with a negative
#	set of sequences, the p-value of a region is calculated
#	using the Fisher exact test on the 
#	enrichment of best matches in the positive sequences relative
#	to the negative sequences, corrected
#	for the number of regions and score thresholds tested.
#	The test assumes that the probability that the best match (if any)
#	falls into a given region
#	is the same for all positive and negative sequences.
# log_adj_p-value:	Log of adjusted p-value.
# bin_location:	Location of the center of the most enriched region.
# bin_width:	The width (in sequence positions) of the most enriched region.
#	A best match to the motif is counted as being in the region if the
#	center of the motif falls in the region.
# total_width:	The window maximal size which can be reached for this motif:
#		rounded(sequence length - motif length +1)/2
# sites_in_bin:	The number of (positive) sequences whose best match to the motif
#	falls in the reported region.
#	Note: This number may be less than the number of
#	(positive) sequences that have a best match in the region.
#	The reason for this is that a sequence may have many matches that score
#	equally best.
#	If n matches have the best score in a sequence, 1/n is added to the
#	appropriate bin for each match.
# total_sites:	The number of sequences containing a match to the motif
#	above the score threshold.
# p_success:	The probability of falling in the enriched window:
#		bin width / total width
# p-value:	The uncorrected p-value before it gets adjusted to the
#	number of multiple tests to give the adjusted p-value.
# mult_tests:	This is the number of multiple tests (n) done for this motif.
#	It was used to correct the original p-value of a region for
#	multiple tests using the formula:
#		p' = 1 - (1-p)^n where p is the uncorrected p-value.
#	The number of multiple tests is the number of regions
#	considered times the number of score thresholds considered.
#	It depends on the motif length, sequence length, and the type of
#	optimizations being done (central enrichment, local enrichment,
#	score optimization).
