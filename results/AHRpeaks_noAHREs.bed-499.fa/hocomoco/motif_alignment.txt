#Query ID	Target ID	Optimal offset	p-value	E-value	q-value	Overlap	Query consensus	Target consensus	Orientation
1	1	0	0	0	0	30	TTATCTTTGTTTTTATTTTATTTGCTTTTG	TTATCTTTGTTTTTATTTTATTTGCTTTTG	+
2	2	0	9.24857e-44	2.31214e-42	4.62428e-42	30	TGCACGTATTGTCTTGATAAACATCTTAAA	TGCACGTATTGTCTTGATAAACATCTTAAA	+
3	3	0	3.74599e-27	9.36498e-26	1.873e-25	11	TGTTTGCTTTT	TGTTTGCTTTT	+
3	22	0	2.11952e-09	5.29881e-08	5.29881e-08	10	TGTTTGCTTTT	TGTTTATTTA	+
3	4	0	3.73944e-09	9.3486e-08	6.2324e-08	7	TGTTTGCTTTT	TGTTTGT	-
3	10	0	1.17893e-08	2.94733e-07	1.47366e-07	11	TGTTTGCTTTT	TGTTTGCTTTGC	+
3	7	0	1.4956e-07	3.73901e-06	1.44859e-06	11	TGTTTGCTTTT	TGTTTACTTTGG	+
3	16	0	1.7383e-07	4.34576e-06	1.44859e-06	11	TGTTTGCTTTT	TGTTTACTTTT	+
3	23	1	2.43312e-06	6.0828e-05	1.73794e-05	10	TGTTTGCTTTT	ATGTTTATTTT	+
3	8	0	5.06935e-06	0.000126734	3.16834e-05	11	TGTTTGCTTTT	TGTTTACTTAGC	+
3	13	2	6.28229e-06	0.000157057	3.49016e-05	11	TGTTTGCTTTT	CTTGTTTACTTTG	+
3	18	1	0.000421986	0.0105496	0.00210993	8	TGTTTGCTTTT	CTGTTTACA	+
3	19	2	0.00148906	0.0372266	0.00676847	8	TGTTTGCTTTT	CTTGTTTACA	+
3	17	4	0.00215917	0.0539793	0.00899655	9	TGTTTGCTTTT	AACTTGTTTACGT	+
3	25	5	0.00285101	0.0712752	0.0109654	11	TGTTTGCTTTT	GTCAATATTTACATAAC	+
4	4	0	7.38067e-15	1.84517e-13	3.69034e-13	7	ACAAACA	ACAAACA	+
4	3	4	5.68301e-08	1.42075e-06	1.42075e-06	7	ACAAACA	AAAAGCAAACA	-
4	22	3	7.87295e-07	1.96824e-05	1.31216e-05	7	ACAAACA	TAAATAAACA	-
4	23	3	0.000140858	0.00352146	0.00176073	7	ACAAACA	AAAATAAACAT	-
4	10	5	0.000270578	0.00676445	0.00270578	7	ACAAACA	GCAAAGCAAACA	-
4	7	5	0.000341698	0.00854246	0.00284749	7	ACAAACA	CCAAAGTAAACA	-
4	8	5	0.000666352	0.0166588	0.00475966	7	ACAAACA	GCTAAGTAAACA	-
4	16	4	0.000952039	0.023801	0.00595024	7	ACAAACA	AAAAGTAAACA	-
4	18	1	0.00197461	0.0493652	0.01097	7	ACAAACA	TGTAAACAG	-
4	19	1	0.0029168	0.0729199	0.014584	7	ACAAACA	TGTAAACAAG	-
4	13	4	0.00326584	0.081646	0.0148447	7	ACAAACA	CAAAGTAAACAAG	-
4	17	2	0.00637428	0.159357	0.0265595	7	ACAAACA	ACGTAAACAAGTT	-
4	25	5	0.00942082	0.23552	0.0362339	7	ACAAACA	GTTATGTAAATATTGAC	-
4	6	1	0.0241148	0.60287	0.0861242	7	ACAAACA	AGAAAACAGGGTTCAAGAGCAGAGAACCAG	+
5	5	0	0	0	0	21	TCTTTCCCTAAGGCAATGTCT	TCTTTCCCTAAGGCAATGTCT	+
6	6	0	0	0	0	30	AGAAAACAGGGTTCAAGAGCAGAGAACCAG	AGAAAACAGGGTTCAAGAGCAGAGAACCAG	+
7	7	0	9.26345e-31	2.31586e-29	4.63173e-29	12	TGTTTACTTTGG	TGTTTACTTTGG	+
7	8	0	1.15292e-16	2.8823e-15	2.8823e-15	12	TGTTTACTTTGG	TGTTTACTTAGC	+
7	13	2	3.77252e-12	9.43129e-11	6.28753e-11	11	TGTTTACTTTGG	CTTGTTTACTTTG	+
7	10	0	7.37612e-12	1.84403e-10	9.22015e-11	12	TGTTTACTTTGG	TGTTTGCTTTGC	+
7	16	0	7.7692e-11	1.9423e-09	7.7692e-10	11	TGTTTACTTTGG	TGTTTACTTTT	+
7	3	0	7.25097e-08	1.81274e-06	6.04247e-07	11	TGTTTACTTTGG	TGTTTGCTTTT	+
7	18	1	9.44903e-07	2.36226e-05	6.68135e-06	8	TGTTTACTTTGG	CTGTTTACA	+
7	19	2	1.06902e-06	2.67254e-05	6.68135e-06	8	TGTTTACTTTGG	CTTGTTTACA	+
7	25	5	1.26519e-06	3.16298e-05	7.02885e-06	12	TGTTTACTTTGG	GTCAATATTTACATAAC	+
7	23	1	2.14433e-06	5.36083e-05	1.07217e-05	10	TGTTTACTTTGG	ATGTTTATTTT	+
7	22	0	7.32876e-06	0.000183219	3.33125e-05	10	TGTTTACTTTGG	TGTTTATTTA	+
7	17	4	2.24786e-05	0.000561966	9.3661e-05	9	TGTTTACTTTGG	AACTTGTTTACGT	+
7	4	0	6.86742e-05	0.00171686	0.000264132	7	TGTTTACTTTGG	TGTTTGT	-
7	6	22	0.0140725	0.351813	0.050259	8	TGTTTACTTTGG	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
8	8	0	3.22282e-31	8.05705e-30	1.61141e-29	12	TGTTTACTTAGC	TGTTTACTTAGC	+
8	7	0	7.59365e-17	1.89841e-15	1.89841e-15	12	TGTTTACTTAGC	TGTTTACTTTGG	+
8	13	2	2.03009e-11	5.07521e-10	3.38348e-10	11	TGTTTACTTAGC	CTTGTTTACTTTG	+
8	10	0	3.69385e-10	9.23462e-09	4.61731e-09	12	TGTTTACTTAGC	TGTTTGCTTTGC	+
8	16	0	1.97198e-09	4.92996e-08	1.97198e-08	11	TGTTTACTTAGC	TGTTTACTTTT	+
8	19	2	6.66316e-07	1.66579e-05	4.90332e-06	8	TGTTTACTTAGC	CTTGTTTACA	+
8	25	5	6.86465e-07	1.71616e-05	4.90332e-06	12	TGTTTACTTAGC	GTCAATATTTACATAAC	+
8	18	1	2.08877e-06	5.22193e-05	1.23434e-05	8	TGTTTACTTAGC	CTGTTTACA	+
8	3	0	2.22181e-06	5.55452e-05	1.23434e-05	11	TGTTTACTTAGC	TGTTTGCTTTT	+
8	23	1	1.28305e-05	0.000320762	6.4056e-05	10	TGTTTACTTAGC	ATGTTTATTTT	+
8	17	4	1.40923e-05	0.000352308	6.4056e-05	9	TGTTTACTTAGC	AACTTGTTTACGT	+
8	22	0	3.99064e-05	0.000997661	0.000166277	10	TGTTTACTTAGC	TGTTTATTTA	+
8	4	0	0.000126392	0.0031598	0.000486123	7	TGTTTACTTAGC	TGTTTGT	-
8	6	22	0.0250746	0.626864	0.089552	8	TGTTTACTTAGC	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
9	9	0	1.4013e-45	3.50325e-44	7.00649e-44	30	AAAATTAGAATTACTGATGAGGGTCTATGT	AAAATTAGAATTACTGATGAGGGTCTATGT	+
10	10	0	7.18097e-35	1.79524e-33	3.59049e-33	12	TGTTTGCTTTGC	TGTTTGCTTTGC	+
10	7	0	1.08202e-12	2.70505e-11	2.70505e-11	12	TGTTTGCTTTGC	TGTTTACTTTGG	+
10	8	0	6.95174e-11	1.73794e-09	1.15862e-09	12	TGTTTGCTTTGC	TGTTTACTTAGC	+
10	3	0	1.28731e-09	3.21828e-08	1.60914e-08	11	TGTTTGCTTTGC	TGTTTGCTTTT	+
10	16	0	2.10615e-08	5.26536e-07	2.10615e-07	11	TGTTTGCTTTGC	TGTTTACTTTT	+
10	13	2	3.52272e-08	8.80679e-07	2.9356e-07	11	TGTTTGCTTTGC	CTTGTTTACTTTG	+
10	22	0	2.90142e-06	7.25354e-05	2.07244e-05	10	TGTTTGCTTTGC	TGTTTATTTA	+
10	4	0	2.49579e-05	0.000623946	0.000155987	7	TGTTTGCTTTGC	TGTTTGT	-
10	23	1	2.90229e-05	0.000725571	0.000161238	10	TGTTTGCTTTGC	ATGTTTATTTT	+
10	18	1	5.21814e-05	0.00130453	0.000260907	8	TGTTTGCTTTGC	CTGTTTACA	+
10	19	2	0.000123819	0.00309547	0.000562812	8	TGTTTGCTTTGC	CTTGTTTACA	+
10	25	5	0.000391461	0.00978651	0.00163109	12	TGTTTGCTTTGC	GTCAATATTTACATAAC	+
10	17	4	0.000740492	0.0185123	0.00284804	9	TGTTTGCTTTGC	AACTTGTTTACGT	+
10	6	22	0.00705539	0.176385	0.0251978	8	TGTTTGCTTTGC	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
10	24	3	0.0201654	0.504135	0.067218	12	TGTTTGCTTTGC	TCTTATCTGTCCCCGCCAG	-
10	1	18	0.0271096	0.67774	0.0847175	12	TGTTTGCTTTGC	TTATCTTTGTTTTTATTTTATTTGCTTTTG	+
11	11	0	4.07429e-14	1.01857e-12	2.03715e-12	8	TGACTCAT	TGACTCAT	+
12	12	0	5.91867e-11	1.47967e-09	2.95933e-09	5	CAGGA	CAGGA	+
13	13	0	1.382e-34	3.45499e-33	6.90999e-33	13	CTTGTTTACTTTG	CTTGTTTACTTTG	+
13	7	-2	4.64294e-13	1.16074e-11	1.16074e-11	11	CTTGTTTACTTTG	TGTTTACTTTGG	+
13	8	-2	2.93981e-12	7.34952e-11	4.89968e-11	11	CTTGTTTACTTTG	TGTTTACTTAGC	+
13	19	0	7.43487e-10	1.85872e-08	9.29359e-09	10	CTTGTTTACTTTG	CTTGTTTACA	+
13	16	-2	5.25678e-09	1.31419e-07	5.25678e-08	11	CTTGTTTACTTTG	TGTTTACTTTT	+
13	10	-2	5.03904e-08	1.25976e-06	4.1992e-07	11	CTTGTTTACTTTG	TGTTTGCTTTGC	+
13	17	2	1.82099e-07	4.55247e-06	1.30071e-06	11	CTTGTTTACTTTG	AACTTGTTTACGT	+
13	18	-1	2.90388e-07	7.2597e-06	1.81492e-06	9	CTTGTTTACTTTG	CTGTTTACA	+
13	3	-2	5.28231e-06	0.000132058	2.93462e-05	11	CTTGTTTACTTTG	TGTTTGCTTTT	+
13	23	-1	5.92416e-06	0.000148104	2.96208e-05	11	CTTGTTTACTTTG	ATGTTTATTTT	+
13	25	3	6.77944e-06	0.000169486	3.08157e-05	13	CTTGTTTACTTTG	GTCAATATTTACATAAC	+
13	22	-2	0.000100823	0.00252058	0.000420097	10	CTTGTTTACTTTG	TGTTTATTTA	+
13	4	-2	0.000494693	0.0123673	0.00190267	7	CTTGTTTACTTTG	TGTTTGT	-
13	6	20	0.0244172	0.610431	0.0872044	10	CTTGTTTACTTTG	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
14	14	0	4.69369e-25	1.17342e-23	2.34685e-23	9	GTTGCGTGC	GTTGCGTGC	+
15	15	0	0	0	0	21	GTGTGTGTGGGTGTGTGTGTG	GTGTGTGTGGGTGTGTGTGTG	+
16	16	0	7.45959e-31	1.8649e-29	3.7298e-29	11	TGTTTACTTTT	TGTTTACTTTT	+
16	7	0	1.53717e-10	3.84293e-09	3.84293e-09	11	TGTTTACTTTT	TGTTTACTTTGG	+
16	8	0	5.15684e-09	1.28921e-07	8.59473e-08	11	TGTTTACTTTT	TGTTTACTTAGC	+
16	13	2	9.18247e-09	2.29562e-07	1.14781e-07	11	TGTTTACTTTT	CTTGTTTACTTTG	+
16	3	0	2.69253e-07	6.73132e-06	2.30468e-06	11	TGTTTACTTTT	TGTTTGCTTTT	+
16	10	0	2.76561e-07	6.91404e-06	2.30468e-06	11	TGTTTACTTTT	TGTTTGCTTTGC	+
16	18	1	6.26273e-06	0.000156568	4.47338e-05	8	TGTTTACTTTT	CTGTTTACA	+
16	23	1	8.88735e-06	0.000222184	5.5546e-05	10	TGTTTACTTTT	ATGTTTATTTT	+
16	22	0	1.87787e-05	0.000469468	9.38936e-05	10	TGTTTACTTTT	TGTTTATTTA	+
16	19	2	1.87787e-05	0.000469468	9.38936e-05	8	TGTTTACTTTT	CTTGTTTACA	+
16	25	5	5.2728e-05	0.0013182	0.000239673	11	TGTTTACTTTT	GTCAATATTTACATAAC	+
16	17	4	8.52422e-05	0.00213106	0.000355176	9	TGTTTACTTTT	AACTTGTTTACGT	+
16	4	0	0.000216069	0.00540174	0.000831036	7	TGTTTACTTTT	TGTTTGT	-
17	17	0	1.88416e-32	4.71041e-31	9.42081e-31	13	AACTTGTTTACGT	AACTTGTTTACGT	+
17	19	-2	8.84209e-08	2.21052e-06	2.21052e-06	10	AACTTGTTTACGT	CTTGTTTACA	+
17	13	-2	6.51185e-07	1.62796e-05	1.08531e-05	11	AACTTGTTTACGT	CTTGTTTACTTTG	+
17	18	-3	5.3171e-06	0.000132928	6.64638e-05	9	AACTTGTTTACGT	CTGTTTACA	+
17	8	-4	2.49622e-05	0.000624056	0.000249623	9	AACTTGTTTACGT	TGTTTACTTAGC	+
17	7	-4	3.94375e-05	0.000985936	0.000328645	9	AACTTGTTTACGT	TGTTTACTTTGG	+
17	16	-4	9.86226e-05	0.00246556	0.000704447	9	AACTTGTTTACGT	TGTTTACTTTT	+
17	25	1	0.000130096	0.00325241	0.000813103	13	AACTTGTTTACGT	GTCAATATTTACATAAC	+
17	23	-3	0.000206926	0.00517314	0.00114959	10	AACTTGTTTACGT	ATGTTTATTTT	+
17	4	-4	0.00112251	0.0280627	0.00534006	7	AACTTGTTTACGT	TGTTTGT	-
17	10	-4	0.00117481	0.0293703	0.00534006	9	AACTTGTTTACGT	TGTTTGCTTTGC	+
17	3	-4	0.00244483	0.0611207	0.0101868	9	AACTTGTTTACGT	TGTTTGCTTTT	+
17	22	-4	0.00590056	0.147514	0.0226945	9	AACTTGTTTACGT	TGTTTATTTA	+
18	18	0	6.33649e-24	1.58412e-22	3.16824e-22	9	CTGTTTACA	CTGTTTACA	+
18	19	1	3.25979e-12	8.14948e-11	8.14948e-11	9	CTGTTTACA	CTTGTTTACA	+
18	13	1	6.72696e-07	1.68174e-05	1.12116e-05	9	CTGTTTACA	CTTGTTTACTTTG	+
18	17	3	2.64021e-06	6.60052e-05	3.30026e-05	9	CTGTTTACA	AACTTGTTTACGT	+
18	16	-1	4.09817e-06	0.000102454	4.09817e-05	8	CTGTTTACA	TGTTTACTTTT	+
18	7	-1	7.03633e-06	0.000175908	5.77141e-05	8	CTGTTTACA	TGTTTACTTTGG	+
18	8	-1	8.07997e-06	0.000201999	5.77141e-05	8	CTGTTTACA	TGTTTACTTAGC	+
18	23	0	0.000247569	0.00618922	0.0015473	9	CTGTTTACA	ATGTTTATTTT	+
18	10	-1	0.000367948	0.0091987	0.00204415	8	CTGTTTACA	TGTTTGCTTTGC	+
18	3	-1	0.000841427	0.0210357	0.00420714	8	CTGTTTACA	TGTTTGCTTTT	+
18	4	-1	0.000943462	0.0235865	0.00428846	7	CTGTTTACA	TGTTTGT	-
18	25	4	0.0015549	0.0388726	0.00647877	9	CTGTTTACA	GTCAATATTTACATAAC	+
18	22	-1	0.00310273	0.0775684	0.0119336	8	CTGTTTACA	TGTTTATTTA	+
18	6	21	0.0158636	0.39659	0.0566557	9	CTGTTTACA	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
19	19	0	3.60567e-26	9.01417e-25	1.80283e-24	10	CTTGTTTACA	CTTGTTTACA	+
19	18	-1	3.80372e-11	9.5093e-10	9.5093e-10	9	CTTGTTTACA	CTGTTTACA	+
19	13	0	5.5756e-09	1.3939e-07	9.29267e-08	10	CTTGTTTACA	CTTGTTTACTTTG	+
19	17	2	5.9413e-08	1.48532e-06	7.42662e-07	10	CTTGTTTACA	AACTTGTTTACGT	+
19	8	-2	5.24809e-06	0.000131202	5.24809e-05	8	CTTGTTTACA	TGTTTACTTAGC	+
19	7	-2	1.07083e-05	0.000267709	8.92362e-05	8	CTTGTTTACA	TGTTTACTTTGG	+
19	16	-2	1.41057e-05	0.000352641	0.000100755	8	CTTGTTTACA	TGTTTACTTTT	+
19	23	-1	0.000529021	0.0132255	0.00330638	9	CTTGTTTACA	ATGTTTATTTT	+
19	10	-2	0.000723623	0.0180906	0.00402013	8	CTTGTTTACA	TGTTTGCTTTGC	+
19	4	-2	0.000835206	0.0208801	0.00417603	7	CTTGTTTACA	TGTTTGT	-
19	3	-2	0.00166397	0.0415993	0.00756352	8	CTTGTTTACA	TGTTTGCTTTT	+
19	25	3	0.00199913	0.0499782	0.0083297	10	CTTGTTTACA	GTCAATATTTACATAAC	+
19	22	-2	0.00415361	0.10384	0.0159754	8	CTTGTTTACA	TGTTTATTTA	+
19	6	20	0.00713339	0.178335	0.0254764	10	CTTGTTTACA	CTGGTTCTCTGCTCTTGAACCCTGTTTTCT	-
20	20	0	6.62072e-14	1.65518e-12	3.31036e-12	8	ATTCCAGA	ATTCCAGA	+
21	21	0	4.59582e-33	1.14896e-31	2.29791e-31	12	AACCTGTTTGAC	AACCTGTTTGAC	+
21	12	-1	0.0030997	0.0774925	0.0516617	5	AACCTGTTTGAC	TCCTG	-
22	22	0	8.77221e-25	2.19305e-23	4.38611e-23	10	TGTTTATTTA	TGTTTATTTA	+
22	3	0	1.43043e-08	3.57607e-07	3.57607e-07	10	TGTTTATTTA	TGTTTGCTTTT	+
22	23	1	6.27018e-08	1.56755e-06	1.04503e-06	10	TGTTTATTTA	ATGTTTATTTT	+
22	4	0	6.44205e-07	1.61051e-05	8.05256e-06	7	TGTTTATTTA	TGTTTGT	-
22	10	0	3.74572e-05	0.000936429	0.000312143	10	TGTTTATTTA	TGTTTGCTTTGC	+
22	7	0	3.74572e-05	0.000936429	0.000312143	10	TGTTTATTTA	TGTTTACTTTGG	+
22	16	0	4.95132e-05	0.00123783	0.000353666	10	TGTTTATTTA	TGTTTACTTTT	+
22	8	0	0.000170763	0.00426907	0.00106727	10	TGTTTATTTA	TGTTTACTTAGC	+
22	13	2	0.00025632	0.006408	0.001424	10	TGTTTATTTA	CTTGTTTACTTTG	+
22	18	1	0.00402663	0.100666	0.0201332	8	TGTTTATTTA	CTGTTTACA	+
22	19	2	0.00583233	0.145808	0.0265106	8	TGTTTATTTA	CTTGTTTACA	+
22	17	4	0.012019	0.300475	0.0483848	9	TGTTTATTTA	AACTTGTTTACGT	+
22	25	5	0.0125801	0.314501	0.0483848	10	TGTTTATTTA	GTCAATATTTACATAAC	+
22	1	3	0.0180141	0.450352	0.064336	10	TGTTTATTTA	TTATCTTTGTTTTTATTTTATTTGCTTTTG	+
23	23	0	1.68171e-24	4.20427e-23	8.40854e-23	11	ATGTTTATTTT	ATGTTTATTTT	+
23	22	-1	2.69998e-07	6.74994e-06	6.74994e-06	10	ATGTTTATTTT	TGTTTATTTA	+
23	3	-1	1.92086e-05	0.000480215	0.000215338	10	ATGTTTATTTT	TGTTTGCTTTT	+
23	13	1	2.01946e-05	0.000504866	0.000215338	11	ATGTTTATTTT	CTTGTTTACTTTG	+
23	7	-1	2.15338e-05	0.000538345	0.000215338	10	ATGTTTATTTT	TGTTTACTTTGG	+
23	16	-1	3.87797e-05	0.000969493	0.000323164	10	ATGTTTATTTT	TGTTTACTTTT	+
23	8	-1	0.000108647	0.00271618	0.000776052	10	ATGTTTATTTT	TGTTTACTTAGC	+
23	4	-1	0.000158801	0.00397002	0.000992506	7	ATGTTTATTTT	TGTTTGT	-
23	10	-1	0.000253524	0.00633809	0.00140846	10	ATGTTTATTTT	TGTTTGCTTTGC	+
23	17	3	0.000397221	0.00993052	0.00193315	10	ATGTTTATTTT	AACTTGTTTACGT	+
23	18	0	0.000425292	0.0106323	0.00193315	9	ATGTTTATTTT	CTGTTTACA	+
23	19	1	0.00077482	0.0193705	0.00322842	9	ATGTTTATTTT	CTTGTTTACA	+
23	25	4	0.0188197	0.470493	0.0723835	11	ATGTTTATTTT	GTCAATATTTACATAAC	+
23	1	2	0.0299658	0.749145	0.099886	11	ATGTTTATTTT	TTATCTTTGTTTTTATTTTATTTGCTTTTG	+
23	2	6	0.0299658	0.749145	0.099886	11	ATGTTTATTTT	TTTAAGATGTTTATCAAGACAATACGTGCA	-
24	24	0	0	0	0	19	CTGGCGGGGACAGATAAGA	CTGGCGGGGACAGATAAGA	+
25	25	0	0	0	0	17	GTCAATATTTACATAAC	GTCAATATTTACATAAC	+
25	8	-5	6.97126e-06	0.000174281	0.000174281	12	GTCAATATTTACATAAC	TGTTTACTTAGC	+
25	7	-5	1.32915e-05	0.000332289	0.000221526	12	GTCAATATTTACATAAC	TGTTTACTTTGG	+
25	13	-3	2.14001e-05	0.000535003	0.000267501	13	GTCAATATTTACATAAC	CTTGTTTACTTTG	+
25	16	-5	0.000124123	0.00310308	0.00124123	11	GTCAATATTTACATAAC	TGTTTACTTTT	+
25	17	-1	0.000173321	0.00433302	0.00144434	13	GTCAATATTTACATAAC	AACTTGTTTACGT	+
25	18	-4	0.00116223	0.0290556	0.00742375	9	GTCAATATTTACATAAC	CTGTTTACA	+
25	10	-5	0.00121972	0.0304931	0.00742375	12	GTCAATATTTACATAAC	TGTTTGCTTTGC	+
25	19	-3	0.00133628	0.0334069	0.00742375	10	GTCAATATTTACATAAC	CTTGTTTACA	+
25	4	-5	0.00271686	0.0679214	0.0135843	7	GTCAATATTTACATAAC	TGTTTGT	-
25	3	-5	0.00304431	0.0761078	0.0138378	11	GTCAATATTTACATAAC	TGTTTGCTTTT	+
25	22	-5	0.00551374	0.137844	0.0229739	10	GTCAATATTTACATAAC	TGTTTATTTA	+
25	23	-4	0.00779095	0.194774	0.0299652	11	GTCAATATTTACATAAC	ATGTTTATTTT	+
