Invoking:
  getsize results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa 1> $metrics
Finished invoke:
  name: count_seqs  status: 0  time: 0.022143
Invoking:
  fasta-most -min 50 < results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa 1> $metrics
Finished invoke:
  name: most_seqs  status: 0  time: 0.046633
Invoking:
  fasta-center -dna -len 100 < results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa 1> results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-centered
Finished invoke:
  name: center_seqs  status: 0  time: 0.066006
Invoking:
  fasta-shuffle-letters results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-centered results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-shuffled -kmer 2 -tag -dinuc -dna -seed 1
Finished invoke:
  name: shuffle_seqs  status: 0  time: 0.009336
Invoking:
  fasta-subsample results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-centered 600 -rest results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-discarded -seed 1 1> results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-sampled
Finished invoke:
  name: sample_seqs  status: 0  time: 0.044561
Invoking:
  fasta-get-markov -nostatus -nosummary -dna -m 1 results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background
Finished invoke:
  name: bg  status: 0  time: 0.007929
Invoking:
  meme results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-sampled -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out -mod zoops -nmotifs 7 -minw 5 -maxw 30 -bfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -dna -revcomp -nostatus
Finished invoke:
  name: meme  status: 0  time: 858.003862
Invoking:
  dreme -v 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-centered -n results/AHRpeaks_noAHREs.bed-499.fa/jaspar/seqs-shuffled -e 0.05
Finished invoke:
  name: dreme  status: 0  time: 26.019767
Invoking:
  centrimo -seqlen 499 -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/centrimo_out -bfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -score 5.0 -ethresh 10.0 results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: centrimo  status: 0  time: 18.386713
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: meme_tomtom  status: 0  time: 44.515691
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: dreme_tomtom  status: 0  time: 1.0742
Invoking:
  tomtom -verbosity 1 -text -thresh 0.1 results/AHRpeaks_noAHREs.bed-499.fa/jaspar/combined.meme results/AHRpeaks_noAHREs.bed-499.fa/jaspar/combined.meme 1> results/AHRpeaks_noAHREs.bed-499.fa/jaspar/motif_alignment.txt
Finished invoke:
  name: align  status: 0  time: 2.118694
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_1 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary KTWTYTTTGTTKTTATYKYATTKRVTTTTK results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo1  status: 0  time: 3.671595
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_2 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary TGCAYGTATTGTCTTGATAAACATCTTAAA results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo2  status: 0  time: 2.233971
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_3 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary TRTTTRYTTWK results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo3  status: 0  time: 24.55529
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_4 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary TCYTTSCCTMAGSCARTGTYT results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo4  status: 0  time: 5.715887
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_5 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary AGRAAACAGGGTTYRAGAGCAGAGAACCRG results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo5  status: 0  time: 2.045605
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_6 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary AAAAKYAGAAYYACTGATRAGGGTCTATGT results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo6  status: 0  time: 2.083411
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_7 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary TGACTCAB results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo7  status: 0  time: 6.235795
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_8 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary CASGW results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo8  status: 0  time: 30.385295
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_9 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary RKGDGTGWGKGTGKGKSTGKG results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo9  status: 0  time: 4.178414
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_10 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary MA0647.1 results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo10  status: 0  time: 7.0484
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/spamo_out_11 -bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background -primary ATTCMAGA results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo11  status: 0  time: 3.259098
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_1 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif KTWTYTTTGTTKTTATYKYATTKRVTTTTK results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo1  status: 0  time: 0.368725
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_2 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif TGCAYGTATTGTCTTGATAAACATCTTAAA results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo2  status: 0  time: 0.352929
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_3 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif TRTTTRYTTWK results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo3  status: 0  time: 0.33525
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_4 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif TCYTTSCCTMAGSCARTGTYT results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo4  status: 0  time: 0.350845
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_5 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif AGRAAACAGGGTTYRAGAGCAGAGAACCRG results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo5  status: 0  time: 0.358938
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_6 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif AAAAKYAGAAYYACTGATRAGGGTCTATGT results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo6  status: 0  time: 0.357271
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_7 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif TGACTCAB results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo7  status: 0  time: 0.2888
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_8 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif CASGW results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo8  status: 0  time: 0.270501
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_9 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif RKGDGTGWGKGTGKGKSTGKG results/AHRpeaks_noAHREs.bed-499.fa/jaspar/meme_out/meme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo9  status: 0  time: 0.347083
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_10 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif MA0647.1 /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo10  status: 0  time: 0.537882
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_noAHREs.bed-499.fa/jaspar/fimo_out_11 --bgfile results/AHRpeaks_noAHREs.bed-499.fa/jaspar/background --motif ATTCMAGA results/AHRpeaks_noAHREs.bed-499.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_noAHREs.bed-499.fa/jaspar/AHRpeaks_noAHREs.bed-499.fa
Finished invoke:
  name: fimo11  status: 0  time: 0.282432
Writing output
Invoking:
  meme-chip_html_to_tsv ./results/AHRpeaks_noAHREs.bed-499.fa/jaspar//meme-chip.html ./results/AHRpeaks_noAHREs.bed-499.fa/jaspar//summary.tsv
Finished invoke:
  name: summary  status: 0  time: 0.223702
Done
