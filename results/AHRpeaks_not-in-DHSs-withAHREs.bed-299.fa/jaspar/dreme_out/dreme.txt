# DREME 4.12.0
#     command: dreme -v 1 -oc results/AHRpeaks_not-in-DHSs-withAHREs.bed-299.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_not-in-DHSs-withAHREs.bed-299.fa/jaspar/seqs-centered -n results/AHRpeaks_not-in-DHSs-withAHREs.bed-299.fa/jaspar/seqs-shuffled -e 0.05
#   positives: 22 from results/AHRpeaks_not-in-DHSs-withAHREs.bed-299.fa/jaspar/seqs-centered (Thu May 24 14:20:00 EDT 2018)
#   negatives: 22 from results/AHRpeaks_not-in-DHSs-withAHREs.bed-299.fa/jaspar/seqs-shuffled (Thu May 24 14:20:00 EDT 2018)
#        host: Sudins-iMac.local
#        when: Thu May 24 14:20:04 EDT 2018

MEME version 4.12.0

ALPHABET "DNA" DNA-LIKE
A "Adenine" CC0000 ~ T "Thymine" 008000
C "Cytosine" 0000CC ~ G "Guanine" FFB300
N "Any base" = ACGT
X = ACGT
. = ACGT
V "Not T" = ACG
H "Not G" = ACT
D "Not C" = AGT
B "Not A" = CGT
M "Amino" = AC
R "Purine" = AG
W "Weak" = AT
S "Strong" = CG
Y "Pyrimidine" = CT
K "Keto" = GT
U = T
END ALPHABET

strands: + -

Background letter frequencies (from dataset):
A 0.336 C 0.190 G 0.213 T 0.262


MOTIF AGATA DREME-1

#             Word    RC Word        Pos        Neg    P-value    E-value
# BEST       AGATA      TATCT         15          1   9.2e-006   4.6e-002
#            AGATA      TATCT         15          1   9.2e-006   4.6e-002

letter-probability matrix: alength= 4 w= 5 nsites= 18 E= 4.6e-002
1.000000 0.000000 0.000000 0.000000
0.000000 0.000000 1.000000 0.000000
1.000000 0.000000 0.000000 0.000000
0.000000 0.000000 0.000000 1.000000
1.000000 0.000000 0.000000 0.000000


# Stopping reason: E-value threshold exceeded
#    Running time: 0.43 seconds
