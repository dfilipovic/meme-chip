#Query ID	Target ID	Optimal offset	p-value	E-value	q-value	Overlap	Query consensus	Target consensus	Orientation
1	1	0	9.80909e-45	3.92364e-44	7.84727e-44	22	GCATGGCAAAAGGAACAGGCAG	GCATGGCAAAAGGAACAGGCAG	+
2	2	0	9.86416e-41	3.94566e-40	7.89133e-40	30	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	+
2	4	1	0.0246864	0.0987457	0.0987457	4	ATCTTTGTTTTTATTGCATTTGCTTTTGGG	TATCT	-
3	3	0	0	0	0	27	TCTGGAAGATAACATCAGAAAAACCCT	TCTGGAAGATAACATCAGAAAAACCCT	+
4	4	0	2.47911e-05	9.91644e-05	0.000198329	5	AGATA	AGATA	+
