Invoking:
  getsize results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa 1> $metrics
Finished invoke:
  name: count_seqs  status: 0  time: 0.019573
Invoking:
  fasta-most -min 50 < results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa 1> $metrics
Finished invoke:
  name: most_seqs  status: 0  time: 0.044479
Invoking:
  fasta-center -dna -len 100 < results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa 1> results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-centered
Finished invoke:
  name: center_seqs  status: 0  time: 0.054707
Invoking:
  fasta-shuffle-letters results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-centered results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-shuffled -kmer 2 -tag -dinuc -dna -seed 1
Finished invoke:
  name: shuffle_seqs  status: 0  time: 0.014006
Invoking:
  fasta-subsample results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-centered 600 -rest results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-discarded -seed 1 1> results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-sampled
Finished invoke:
  name: sample_seqs  status: 0  time: 0.046863
Invoking:
  fasta-get-markov -nostatus -nosummary -dna -m 1 results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background
Finished invoke:
  name: bg  status: 0  time: 0.007158
Invoking:
  meme results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-sampled -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out -mod zoops -nmotifs 7 -minw 5 -maxw 30 -bfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -dna -revcomp -nostatus
Finished invoke:
  name: meme  status: 0  time: 837.422915
Invoking:
  dreme -v 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out -png -dna -p results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-centered -n results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/seqs-shuffled -e 0.05
Finished invoke:
  name: dreme  status: 0  time: 60.353978
Invoking:
  centrimo -seqlen 299 -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/centrimo_out -bfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -score 5.0 -ethresh 10.0 results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: centrimo  status: 0  time: 8.960018
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: meme_tomtom  status: 0  time: 20.017908
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: dreme_tomtom  status: 0  time: 1.117753
Invoking:
  tomtom -verbosity 1 -text -thresh 0.1 results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/combined.meme results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/combined.meme 1> results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/motif_alignment.txt
Finished invoke:
  name: align  status: 0  time: 0.590916
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_1 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary DCACGC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo1  status: 0  time: 0.47697
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_2 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary RYAAAYA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo2  status: 0  time: 0.454129
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_3 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary GCTCACTGCAACCTCCRCCTCCYRGGYTCA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo3  status: 0  time: 0.504249
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_4 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary TGACTCAB results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo4  status: 0  time: 0.450573
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_5 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary CMCAGR results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo5  status: 0  time: 0.445498
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_6 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary TTATCW results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo6  status: 0  time: 0.452731
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_7 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary CABGSTGGBDCAYGCMTGTAATCCCAGCWM results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo7  status: 0  time: 0.503638
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_8 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary CMSSTSWCCCWSCCYC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo8  status: 0  time: 0.510573
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_9 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary RGRGAAA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo9  status: 0  time: 0.445259
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_10 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary AWGATCRYGCCACTGCACTCCA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo10  status: 0  time: 0.506521
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/spamo_out_11 -bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background -primary AGGTSAC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme
Finished invoke:
  name: spamo11  status: 0  time: 0.442727
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_1 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif DCACGC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo1  status: 0  time: 0.243436
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_2 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif RYAAAYA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo2  status: 0  time: 0.249752
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_3 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif GCTCACTGCAACCTCCRCCTCCYRGGYTCA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo3  status: 0  time: 0.303488
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_4 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif TGACTCAB results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo4  status: 0  time: 0.242455
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_5 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif CMCAGR results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo5  status: 0  time: 0.237832
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_6 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif TTATCW results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo6  status: 0  time: 0.23434
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_7 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif CABGSTGGBDCAYGCMTGTAATCCCAGCWM results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo7  status: 0  time: 0.301602
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_8 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif CMSSTSWCCCWSCCYC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo8  status: 0  time: 0.297158
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_9 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif RGRGAAA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo9  status: 0  time: 0.249319
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_10 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif AWGATCRYGCCACTGCACTCCA results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/meme_out/meme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo10  status: 0  time: 0.298621
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/fimo_out_11 --bgfile results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/background --motif AGGTSAC results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/dreme_out/dreme.xml results/AHRpeaks_withAHREs.bed-299.fa/hocomoco/AHRpeaks_withAHREs.bed-299.fa
Finished invoke:
  name: fimo11  status: 0  time: 0.242731
Writing output
Invoking:
  meme-chip_html_to_tsv ./results/AHRpeaks_withAHREs.bed-299.fa/hocomoco//meme-chip.html ./results/AHRpeaks_withAHREs.bed-299.fa/hocomoco//summary.tsv
Finished invoke:
  name: summary  status: 0  time: 0.132669
Done
