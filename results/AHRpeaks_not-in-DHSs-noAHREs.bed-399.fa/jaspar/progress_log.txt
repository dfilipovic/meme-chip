Invoking:
  getsize results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa 1> $metrics
Finished invoke:
  name: count_seqs  status: 0  time: 0.006335
Invoking:
  fasta-most -min 50 < results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa 1> $metrics
Finished invoke:
  name: most_seqs  status: 0  time: 0.036428
Invoking:
  fasta-center -dna -len 100 < results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa 1> results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-centered
Finished invoke:
  name: center_seqs  status: 0  time: 0.041169
Invoking:
  fasta-shuffle-letters results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-centered results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-shuffled -kmer 2 -tag -dinuc -dna -seed 1
Finished invoke:
  name: shuffle_seqs  status: 0  time: 0.003498
Invoking:
  fasta-get-markov -nostatus -nosummary -dna -m 1 results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background
Finished invoke:
  name: bg  status: 0  time: 0.003289
Invoking:
  meme results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-centered -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out -mod zoops -nmotifs 7 -minw 5 -maxw 30 -bfile results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background -dna -revcomp -nostatus
Finished invoke:
  name: meme  status: 0  time: 7.174745
Invoking:
  dreme -v 1 -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-centered -n results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/seqs-shuffled -e 0.05
Finished invoke:
  name: dreme  status: 0  time: 0.414527
Invoking:
  centrimo -seqlen 399 -verbosity 1 -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/centrimo_out -bfile results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background -score 5.0 -ethresh 10.0 results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: centrimo  status: 0  time: 1.738049
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: meme_tomtom  status: 0  time: 18.355055
Invoking:
  tomtom -verbosity 1 -text -thresh 0.1 results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/combined.meme results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/combined.meme 1> results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/motif_alignment.txt
Finished invoke:
  name: align  status: 0  time: 0.020151
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/spamo_out_1 -bgfile results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background -primary SWSTGTGTGCSTGCSTSMGTG results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo1  status: 0  time: 1.84206
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/fimo_out_1 --bgfile results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/background --motif SWSTGTGTGCSTGCSTSMGTG results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/meme_out/meme.xml results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa
Finished invoke:
  name: fimo1  status: 0  time: 0.046477
Writing output
Invoking:
  meme-chip_html_to_tsv ./results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar//meme-chip.html ./results/AHRpeaks_not-in-DHSs-noAHREs.bed-399.fa/jaspar//summary.tsv
Finished invoke:
  name: summary  status: 0  time: 0.096627
Done
