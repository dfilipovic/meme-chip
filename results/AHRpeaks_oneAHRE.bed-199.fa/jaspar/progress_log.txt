Invoking:
  getsize results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa 1> $metrics
Finished invoke:
  name: count_seqs  status: 0  time: 0.010868
Invoking:
  fasta-most -min 50 < results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa 1> $metrics
Finished invoke:
  name: most_seqs  status: 0  time: 0.039168
Invoking:
  fasta-center -dna -len 100 < results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa 1> results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-centered
Finished invoke:
  name: center_seqs  status: 0  time: 0.046384
Invoking:
  fasta-shuffle-letters results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-centered results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-shuffled -kmer 2 -tag -dinuc -dna -seed 1
Finished invoke:
  name: shuffle_seqs  status: 0  time: 0.006033
Invoking:
  fasta-subsample results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-centered 600 -rest results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-discarded -seed 1 1> results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-sampled
Finished invoke:
  name: sample_seqs  status: 0  time: 0.042319
Invoking:
  fasta-get-markov -nostatus -nosummary -dna -m 1 results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background
Finished invoke:
  name: bg  status: 0  time: 0.004007
Invoking:
  meme results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-sampled -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out -mod zoops -nmotifs 7 -minw 5 -maxw 30 -bfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -dna -revcomp -nostatus
Finished invoke:
  name: meme  status: 0  time: 816.353274
Invoking:
  dreme -v 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out -png -dna -p results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-centered -n results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/seqs-shuffled -e 0.05
Finished invoke:
  name: dreme  status: 0  time: 33.627789
Invoking:
  centrimo -seqlen 199 -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/centrimo_out -bfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -score 5.0 -ethresh 10.0 results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: centrimo  status: 0  time: 6.405536
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: meme_tomtom  status: 0  time: 37.201921
Invoking:
  tomtom -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_tomtom_out -min-overlap 5 -dist pearson -evalue -thresh 1 -no-ssc -bfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: dreme_tomtom  status: 0  time: 1.321115
Invoking:
  tomtom -verbosity 1 -text -thresh 0.1 results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/combined.meme results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/combined.meme 1> results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/motif_alignment.txt
Finished invoke:
  name: align  status: 0  time: 0.559449
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_1 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary DCACGC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo1  status: 0  time: 0.720694
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_2 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary MARAGTVAAANWMAATHWMAAAAAAAAAAA results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo2  status: 0  time: 0.753301
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_3 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary SARCCCAGGHRRSBGHGKRDGSWSTGAGCC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo3  status: 0  time: 0.761935
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_4 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary GTDTRTGTGYGTGTKT results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo4  status: 0  time: 0.754561
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_5 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary TRTTTR results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo5  status: 0  time: 0.706901
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_6 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary GKCCYSAGGGCW results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo6  status: 0  time: 0.758808
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_7 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary TGACTCA results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo7  status: 0  time: 0.693445
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_8 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary ATCTYTC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo8  status: 0  time: 0.687371
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_9 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary YGGTRGCWCAYGMYTGTARTCYCAGCWMYT results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo9  status: 0  time: 0.755497
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_10 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary TTATCW results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo10  status: 0  time: 0.688033
Invoking:
  spamo -verbosity 1 -oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/spamo_out_11 -bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background -primary CCGCCYC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml /Users/filipov4/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme
Finished invoke:
  name: spamo11  status: 0  time: 0.696355
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_1 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif DCACGC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo1  status: 0  time: 0.109936
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_2 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif MARAGTVAAANWMAATHWMAAAAAAAAAAA results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo2  status: 0  time: 0.165187
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_3 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif SARCCCAGGHRRSBGHGKRDGSWSTGAGCC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo3  status: 0  time: 0.164678
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_4 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif GTDTRTGTGYGTGTKT results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo4  status: 0  time: 0.164702
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_5 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif TRTTTR results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo5  status: 0  time: 0.100048
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_6 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif GKCCYSAGGGCW results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo6  status: 0  time: 0.164092
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_7 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif TGACTCA results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo7  status: 0  time: 0.111949
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_8 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif ATCTYTC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo8  status: 0  time: 0.114958
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_9 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif YGGTRGCWCAYGMYTGTARTCYCAGCWMYT results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/meme_out/meme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo9  status: 0  time: 0.173357
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_10 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif TTATCW results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo10  status: 0  time: 0.100331
Invoking:
  fimo --parse-genomic-coord --verbosity 1 --oc results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/fimo_out_11 --bgfile results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/background --motif CCGCCYC results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/dreme_out/dreme.xml results/AHRpeaks_oneAHRE.bed-199.fa/jaspar/AHRpeaks_oneAHRE.bed-199.fa
Finished invoke:
  name: fimo11  status: 0  time: 0.109068
Writing output
Invoking:
  meme-chip_html_to_tsv ./results/AHRpeaks_oneAHRE.bed-199.fa/jaspar//meme-chip.html ./results/AHRpeaks_oneAHRE.bed-199.fa/jaspar//summary.tsv
Finished invoke:
  name: summary  status: 0  time: 0.123075
Done
