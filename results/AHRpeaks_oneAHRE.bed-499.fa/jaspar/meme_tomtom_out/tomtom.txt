#Query ID	Target ID	Optimal offset	p-value	E-value	q-value	Overlap	Query consensus	Target consensus	Orientation
SCCYCWGGS	MA0003.3	1	1.04751e-06	0.00147071	0.00293093	9	GCCTCAGGC	CGCCTCAGGCA	+
SCCYCWGGS	MA0812.1	1	2.85947e-06	0.0040147	0.00400038	9	GCCTCAGGC	AGCCTCAGGCA	+
SCCYCWGGS	MA0814.1	1	9.23839e-06	0.0129707	0.00861631	9	GCCTCAGGC	AGCCTCAGGCA	+
SCCYCWGGS	MA0524.2	1	0.00016903	0.237318	0.0675635	9	GCCTCAGGC	TGCCCCAGGGCA	+
SCCYCWGGS	MA0811.1	1	0.000205816	0.288966	0.071984	9	GCCTCAGGC	TGCCCCAGGGCA	+
SCCYCWGGS	MA0872.1	2	0.000343129	0.481753	0.106675	9	GCCTCAGGC	TGCCCTCAGGGCA	-
SCCYCWGGS	MA0813.1	2	0.000466088	0.654387	0.118555	9	GCCTCAGGC	TGCCCTCAGGGCA	-
SCCYCWGGS	MA0815.1	2	0.000466088	0.654387	0.118555	9	GCCTCAGGC	TGCCCTCAGGGCA	-
SCCYCWGGS	MA0810.1	2	0.000615381	0.863994	0.134987	9	GCCTCAGGC	TGCCCCCGGGCA	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1267.1	0	2.28951e-10	3.21447e-07	6.31581e-07	29	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTTTTTTTACTTTTTTTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1268.1	-1	1.20855e-09	1.6968e-06	1.66694e-06	27	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTCACTTTTTCTTTTTTTTTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1274.1	-3	2.02273e-08	2.83991e-05	1.85995e-05	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTACTTTTTTTTTTTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1281.1	-9	2.8963e-07	0.00040664	0.000199742	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTTTTTACTTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1279.1	-1	6.54313e-07	0.000918656	0.000360996	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTCACTTTTTCTTTTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1272.1	-2	1.801e-06	0.0025286	0.000724642	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTTTTTACTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1273.1	1	1.8388e-06	0.00258168	0.000724642	18	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTGCCTTTTTTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1277.1	-7	4.08318e-06	0.00573278	0.001346	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTACTTTTTCTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1278.1	-1	4.39138e-06	0.0061655	0.001346	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTTTTTTACTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1159.1	0	1.2689e-05	0.0178154	0.00350038	15	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTGTCTTTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1270.1	-3	6.29194e-05	0.0883388	0.015779	19	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTACTTTTTGCTTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1125.1	-8	0.000111745	0.15689	0.0256883	12	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTAAA	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1158.1	-9	0.000167727	0.235488	0.0355914	20	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTGTCGTTTTCTG	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1156.1	-9	0.000201694	0.283178	0.0397421	20	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTGTCGTTTTCTG	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1160.1	-9	0.000242062	0.339855	0.0445166	20	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTGTCGTTTTGTG	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1157.1	-9	0.000289946	0.407084	0.0496804	20	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTTTTGTCGTTTTGTG	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA0036.3	0	0.000306159	0.429848	0.0496804	11	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTCTTATCTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA0846.1	-8	0.000395988	0.555968	0.0591174	12	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTGTTTACTTA	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA0593.1	-8	0.000407177	0.571676	0.0591174	11	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTGTTTACTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1371.1	-12	0.000432401	0.607091	0.0596408	17	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTGTCGTTTTGTG	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1205.1	-12	0.000550573	0.773005	0.072324	18	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTTTTCCTTTTTTGGAAA	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1317.1	-7	0.00060119	0.844071	0.0734105	13	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	CTTTGACTTTTTT	+
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA0277.1	-4	0.000612068	0.859343	0.0734105	9	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	TTTCTTTTT	-
TTTTHWTCTTTKTTTTYTTTDTTYTTGWWT	MA1193.1	-2	0.000694628	0.975257	0.0798413	21	TTTTTATCTTTGTTTTTTTTTTTTTTGTTT	CTTATCCTTATCCAATTTTTT	-
TGTGTGYGTGTGTRTGTKTGTG	MA0538.1	-7	1.5623e-07	0.000219347	0.000438694	15	TGTGTGTGTGTGTATGTGTGTG	GTGTGTGTGTGCGTG	+
CHSKSTSHCCYHSCYCCYHCCC	MA0528.1	-2	7.27874e-07	0.00102194	0.00203585	20	CACTGTCCCCTCCCCCCTCCCC	TCCTCCTCCCCCTCCTCCTCC	-
CHSKSTSHCCYHSCYCCYHCCC	MA0516.1	-6	3.33372e-06	0.00468055	0.00466219	15	CACTGTCCCCTCCCCCCTCCCC	GCCCCGCCCCCTCCC	+
CHSKSTSHCCYHSCYCCYHCCC	MA1402.1	0	1.82967e-05	0.0256886	0.0152675	21	CACTGTCCCCTCCCCCCTCCCC	CTCTCTCTCTCTCTCTCTCTC	+
CHSKSTSHCCYHSCYCCYHCCC	MA1404.1	1	2.22419e-05	0.0312276	0.0152675	22	CACTGTCCCCTCCCCCCTCCCC	TCTCTCTCTCTCTCTCTCTCTCTC	-
CHSKSTSHCCYHSCYCCYHCCC	MA1284.1	-1	2.72928e-05	0.0383191	0.0152675	21	CACTGTCCCCTCCCCCCTCCCC	TGTGGGCCCCACCTCCTCTCTTGGGCCTAT	+
CHSKSTSHCCYHSCYCCYHCCC	MA0753.1	-12	6.71173e-05	0.0942326	0.0283422	10	CACTGTCCCCTCCCCCCTCCCC	CCCCCCCCAC	+
CHSKSTSHCCYHSCYCCYHCCC	MA1403.1	6	7.09318e-05	0.0995882	0.0283422	22	CACTGTCCCCTCCCCCCTCCCC	CTCTCTCTCTCTCTCTCTCTCTCTCTCTCT	-
CHSKSTSHCCYHSCYCCYHCCC	MA0079.3	-6	0.000137074	0.192452	0.0479244	11	CACTGTCCCCTCCCCCCTCCCC	GCCCCGCCCCC	+
CHSKSTSHCCYHSCYCCYHCCC	MA0057.1	-10	0.000213433	0.299659	0.0663298	10	CACTGTCCCCTCCCCCCTCCCC	TTCCCCCTAC	-
CHSKSTSHCCYHSCYCCYHCCC	MA0543.1	-1	0.000296348	0.416073	0.0828881	15	CACTGTCCCCTCCCCCCTCCCC	TCTCTGCGTCTCTCT	-
CHSKSTSHCCYHSCYCCYHCCC	MA0065.2	-5	0.000516691	0.725434	0.13138	15	CACTGTCCCCTCCCCCCTCCCC	TGACCTTTGCCCTAC	-
CHSKSTSHCCYHSCYCCYHCCC	MA1283.1	-2	0.000686915	0.964429	0.160108	20	CACTGTCCCCTCCCCCCTCCCC	GTGGGTCCCACATCCTCTGT	-
TSTGRKTTKTSTKTKTACTSTG	MA0277.1	-7	0.000191248	0.268513	0.536456	9	TGTGGGTTGTCTTTTTACTCTG	TTTCTTTTT	-
