# WARNING: this file is not sorted!
# db	id                         alt	consensus	 E-value	adj_p-value	log_adj_p-value	bin_location	bin_width	total_width	sites_in_bin	total_sites	p_success	 p-value	mult_tests
   1	CAYTHWRYAAATATTTRCTGAR  MEME-4	CAYTHWRYAAATATTTRCTGAR	4.3e0000	   3.0e-003	          -5.80	         0.0	      148	        478	         148	        355	  0.30962	1.3e-005	       238
   1	SCCTGAGGC               MEME-6	SCCTGAGGC	1.6e0000	   1.1e-003	          -6.78	         0.0	      233	        491	         315	        553	  0.47454	4.6e-006	       245
   1	WGDCACGCA               MEME-7	WGDCACGCA	1.1e-008	   7.6e-012	         -25.60	         0.0	      281	        491	         392	        537	  0.57230	3.1e-014	       245
   2	DCACGC                 DREME-1	DCACGC	1.7e-022	   1.2e-025	         -57.37	         0.0	      230	        494	         629	        983	  0.46559	4.9e-028	       246
   2	RYAAAYA                DREME-2	RYAAAYA	2.8e-001	   2.0e-004	          -8.54	         0.0	      257	        493	         560	        934	  0.52130	8.0e-007	       246
   2	RTGAGTCA               DREME-3	RTGAGTCA	2.2e-003	   1.6e-006	         -13.37	         0.0	      306	        492	         164	        202	  0.62195	6.4e-009	       245
   2	CGTGH                  DREME-7	CGTGH	1.4e-017	   9.9e-021	         -46.06	         0.0	      231	        495	         699	       1142	  0.46667	4.0e-023	       247
   3	MA0004.1                  Arnt	CACGTG	2.4e-001	   1.7e-004	          -8.68	         0.0	      248	        494	         312	        513	  0.50202	6.9e-007	       246
   3	MA0006.1             Ahr::Arnt	YGCGTG	3.3e-017	   2.3e-020	         -45.21	         0.0	      228	        494	         712	       1179	  0.46154	9.5e-023	       246
   3	MA0030.1                 FOXF2	BNAASGTAAACAAD	4.0e0000	   2.8e-003	          -5.86	         0.0	      198	        486	         276	        554	  0.40741	1.2e-005	       242
   3	MA0067.1                  Pax2	NGTCAYKB	9.1e-003	   6.4e-006	         -11.96	         0.0	      216	        492	         628	       1214	  0.43902	2.6e-008	       245
   3	MA0089.1          MAFG::NFE2L1	NATGAC	2.9e-002	   2.1e-005	         -10.78	         0.0	      220	        494	         631	       1212	  0.44534	8.4e-008	       246
   3	MA0047.2                 Foxa2	TGTTTACWYWGB	4.5e-003	   3.1e-006	         -12.67	         0.0	      274	        488	         653	       1008	  0.56148	1.3e-008	       243
   3	MA0259.1           ARNT::HIF1A	VBACGTGC	1.1e-006	   7.5e-010	         -21.01	         0.0	      210	        492	         499	        923	  0.42683	3.1e-012	       245
   3	MA0289.1                 DAL80	CGATAAG	3.3e-001	   2.3e-004	          -8.38	         0.0	      219	        493	         406	        765	  0.44422	9.4e-007	       246
   3	MA0291.1                 DAL82	AWWDTGCGC	1.7e-004	   1.2e-007	         -15.94	         0.0	      361	        491	         527	        629	  0.73523	4.9e-010	       245
   3	MA0295.1                  FHL1	GACGCAMA	1.9e-018	   1.4e-021	         -48.04	         0.0	      348	        492	         700	        816	  0.70732	5.6e-024	       245
   3	MA0297.1                  FKH2	GTAAACA	3.4e0000	   2.4e-003	          -6.02	         0.0	      317	        493	         275	        367	  0.64300	9.9e-006	       246
   3	MA0300.1                  GAT1	YYGATAAG	4.1e-003	   2.9e-006	         -12.74	         0.0	      218	        492	         358	        647	  0.44309	1.2e-008	       245
   3	MA0307.1                  GLN3	GATAA	2.4e-002	   1.7e-005	         -11.00	         0.0	      293	        495	         767	       1148	  0.59192	6.8e-008	       247
   3	MA0329.1                  MBP1	ACGCGWN	3.0e-008	   2.1e-011	         -24.59	         0.0	      343	        493	         690	        854	  0.69574	8.5e-014	       246
   3	MA0401.1                  SWI4	ACGCGAAA	2.6e-001	   1.8e-004	          -8.61	         0.0	      102	        492	          78	        224	  0.20732	7.4e-007	       245
   3	MA0419.1                  YAP7	ATTAGTAAKCA	5.6e0000	   4.0e-003	          -5.53	         0.0	      221	        489	         356	        668	  0.45194	1.6e-005	       244
   3	MA0446.1                   fkh	TGTTTRYHYAA	1.5e0000	   1.1e-003	          -6.85	         0.0	      359	        489	         820	       1032	  0.73415	4.4e-006	       244
   3	MA0458.1                  slp1	DTGTTTAYRYW	4.7e-001	   3.3e-004	          -8.01	         0.0	      199	        489	         534	       1119	  0.40695	1.4e-006	       244
   3	MA0501.1             MAF::NFE2	ATGACTCAGCANWTY	4.8e-003	   3.4e-006	         -12.60	         0.0	      211	        485	         257	        453	  0.43505	1.4e-008	       242
   3	MA0491.1                  JUND	DRTGASTCATS	2.8e-004	   2.0e-007	         -15.45	         0.0	      299	        489	         310	        412	  0.61145	8.0e-010	       244
   3	MA0490.1                  JUNB	NRRTGASTCAK	3.7e-004	   2.6e-007	         -15.15	         0.0	      221	        489	         282	        479	  0.45194	1.1e-009	       244
   3	MA0489.1            JUN(var.2)	RRRRRRTGASTCAT	3.5e-002	   2.5e-005	         -10.62	         0.0	      232	        486	         351	        601	  0.47737	1.0e-007	       242
   3	MA0478.1                 FOSL2	KRRTGASTCAB	3.4e-001	   2.4e-004	          -8.34	         0.0	      221	        489	         327	        594	  0.45194	9.8e-007	       244
   3	MA0477.1                 FOSL1	RRTGASTCAKV	1.8e-003	   1.3e-006	         -13.59	         0.0	      299	        489	         335	        453	  0.61145	5.1e-009	       244
   3	MA0476.1                   FOS	DVTGASTCATB	1.1e-001	   7.7e-005	          -9.47	         0.0	      317	        489	         395	        527	  0.64826	3.2e-007	       244
   3	MA0462.1             BATF::JUN	RRWATGASTCA	5.6e-001	   3.9e-004	          -7.84	         0.0	      217	        489	         355	        664	  0.44376	1.6e-006	       244
   3	MA0527.1                ZBTB33	BTCTCGCGAGABYTS	4.9e0000	   3.5e-003	          -5.66	         0.0	      253	        485	         140	        210	  0.52165	1.4e-005	       242
   3	MA0148.3                 FOXA1	YNBNTGTTTACWYWD	1.2e-001	   8.6e-005	          -9.36	         0.0	      433	        485	         838	        890	  0.89278	3.6e-007	       242
   3	MA0035.3                 Gata1	TYCTTATCTSY	1.7e-003	   1.2e-006	         -13.66	         0.0	      201	        489	         300	        564	  0.41104	4.8e-009	       244
   3	MA0150.2                Nfe2l2	CASNATGACTCAGCA	1.8e-004	   1.3e-007	         -15.88	         0.0	      299	        485	         483	        661	  0.61649	5.2e-010	       242
   3	MA0538.1                daf-12	GWGYGYGTGTGYGTB	7.9e0000	   5.6e-003	          -5.19	         0.0	      331	        485	         428	        562	  0.68247	2.3e-005	       242
   3	MA0546.1                 pha-4	WRWGYAAAYA	7.9e-001	   5.6e-004	          -7.49	         0.0	      178	        490	         326	        730	  0.36327	2.3e-006	       244
   3	MA0557.1                  FHY3	HHCACGCGCTNN	6.3e-004	   4.4e-007	         -14.63	         0.0	      260	        488	         444	        689	  0.53279	1.8e-009	       243
   3	MA0591.1           Bach1::Mafk	RSSATGACTCAGCAB	1.2e0000	   8.4e-004	          -7.08	         0.0	      317	        485	         241	        312	  0.65361	3.5e-006	       242
   3	MA0593.1                 FOXP2	RWGTAAACAVR	2.6e0000	   1.8e-003	          -6.31	         0.0	      329	        489	         521	        695	  0.67280	7.5e-006	       244
   3	MA0613.1                 FOXG1	RTAAACAW	5.2e-001	   3.7e-004	          -7.91	         0.0	      316	        492	         277	        365	  0.64228	1.5e-006	       245
   3	MA0615.1                 Gmeb1	BHBBKKACGTMMNWNNN	1.6e0000	   1.1e-003	          -6.78	         0.0	      171	        483	         302	        692	  0.35404	4.7e-006	       241
   3	MA1099.1                  Hes1	SVCACGYGHN	3.6e-002	   2.6e-005	         -10.57	         0.0	      328	        490	         859	       1160	  0.66939	1.1e-007	       244
   3	MA0632.1                 Tcfl5	NBCDCGHGVN	4.7e0000	   3.3e-003	          -5.72	         0.0	      248	        490	         512	        888	  0.50612	1.4e-005	       244
   3	MA0042.2                 FOXI1	GTAAACA	2.2e0000	   1.6e-003	          -6.45	         0.0	      225	        493	         544	       1037	  0.45639	6.4e-006	       246
   3	MA0033.2                 FOXL1	RTAAACA	1.7e-002	   1.2e-005	         -11.33	         0.0	      201	        493	         545	       1118	  0.40771	4.9e-008	       246
   3	MA0157.2                 FOXO3	GTAAACAW	1.0e0000	   7.2e-004	          -7.24	         0.0	      192	        492	         338	        713	  0.39024	2.9e-006	       245
   3	MA0655.1                  JDP2	ATGACTCAT	9.0e-001	   6.4e-004	          -7.36	         0.0	      291	        491	         316	        453	  0.59267	2.6e-006	       245
   3	MA0766.1                 GATA5	WGATAASR	4.2e-001	   2.9e-004	          -8.13	         0.0	      218	        492	         579	       1128	  0.44309	1.2e-006	       245
   3	MA0810.1         TFAP2A(var.2)	YGCCCBVRGGCR	2.2e0000	   1.5e-003	          -6.48	         0.0	      286	        488	         608	        926	  0.58607	6.3e-006	       243
   3	MA0003.3                TFAP2A	HGCCYSAGGCD	2.2e0000	   1.5e-003	          -6.48	         0.0	      257	        489	         560	        938	  0.52556	6.3e-006	       244
   3	MA0811.1                TFAP2B	YGCCCBVRGGCA	1.7e-001	   1.2e-004	          -9.03	         0.0	      258	        488	         556	        911	  0.52869	4.9e-007	       243
   3	MA0812.1         TFAP2B(var.2)	HGCCTSAGGCD	6.7e-001	   4.7e-004	          -7.66	         0.0	      255	        489	         459	        758	  0.52147	1.9e-006	       244
   3	MA0524.2                TFAP2C	YGCCYBVRGGCA	8.0e-001	   5.7e-004	          -7.48	         0.0	      264	        488	         589	        958	  0.54098	2.3e-006	       243
   3	MA0814.1         TFAP2C(var.2)	HGCCTSAGGCD	4.7e0000	   3.3e-003	          -5.71	         0.0	      253	        489	         520	        884	  0.51738	1.4e-005	       244
   3	MA0841.1                  NFE2	VATGACTCATS	1.5e-002	   1.1e-005	         -11.46	         0.0	      305	        489	         181	        229	  0.62372	4.3e-008	       244
   3	MA0847.1                 FOXD2	GTAAACA	1.4e0000	   1.0e-003	          -6.89	         0.0	      201	        493	         562	       1190	  0.40771	4.1e-006	       246
   3	MA0848.1                 FOXO4	GTAAACA	7.1e0000	   5.0e-003	          -5.29	         0.0	      191	        493	         508	       1134	  0.38742	2.1e-005	       246
   3	MA0850.1                 FOXP3	RTAAACA	5.9e-002	   4.1e-005	         -10.09	         0.0	      201	        493	         579	       1203	  0.40771	1.7e-007	       246
   3	MA0851.1                 Foxj3	NDAADGTAAACAAANNM	9.3e0000	   6.6e-003	          -5.03	         0.0	      201	        483	         466	        969	  0.41615	2.7e-005	       241
   3	MA0935.1                NAC025	YACGTAAY	4.1e-003	   2.9e-006	         -12.74	         0.0	      232	        492	         349	        594	  0.47154	1.2e-008	       245
   3	MA0936.1             T11I18.17	ACACGCMA	6.2e-007	   4.3e-010	         -21.56	         0.0	      228	        492	         681	       1207	  0.46341	1.8e-012	       245
   3	MA0937.1                NAC055	ACACGTAA	3.9e-002	   2.8e-005	         -10.50	         0.0	      234	        492	         536	        957	  0.47561	1.1e-007	       245
   3	MA0938.1                NAC058	RCACGCAA	1.2e-012	   8.6e-016	         -34.69	         0.0	      212	        492	         674	       1216	  0.43089	3.5e-018	       245
   3	MA0939.1                NAC080	ACACGCAA	3.7e-006	   2.6e-009	         -19.76	         0.0	      230	        492	         280	        447	  0.46748	1.1e-011	       245
   3	MA0962.1                BHLH34	GCACGTGK	7.6e0000	   5.4e-003	          -5.22	         0.0	      324	        492	         489	        667	  0.65854	2.2e-005	       245
   3	MA0969.1                 CMTA2	NNDVCGCGT	2.5e-005	   1.8e-008	         -17.84	         0.0	      361	        491	         616	        738	  0.73523	7.3e-011	       245
   3	MA0970.1                 CMTA3	CCGCGTNNN	2.1e-005	   1.5e-008	         -18.03	         0.0	      347	        491	         618	        764	  0.70672	6.0e-011	       245
   3	MA1043.1                NAC083	WYACGYAACN	5.0e-004	   3.5e-007	         -14.86	         0.0	      386	        490	         250	        271	  0.78776	1.4e-009	       244
   3	MA1101.1                 BACH2	NMTGACTCAGCANH	2.7e-002	   1.9e-005	         -10.87	         0.0	      202	        486	         438	        867	  0.41564	7.9e-008	       242
   3	MA0852.2                 FOXK1	NRDGTAAACAAGNN	6.5e0000	   4.6e-003	          -5.38	         0.0	      348	        486	         849	       1100	  0.71605	1.9e-005	       242
   3	MA0481.2                 FOXP1	NDGTAAACAGNN	2.6e-002	   1.8e-005	         -10.91	         0.0	      234	        488	         616	       1101	  0.47951	7.5e-008	       243
   3	MA0036.3                 GATA2	NBCTTATCTNH	3.0e-001	   2.1e-004	          -8.45	         0.0	       97	        489	         255	        973	  0.19836	8.8e-007	       244
   3	MA1104.1                 GATA6	NDNAGATAAGADD	4.4e0000	   3.1e-003	          -5.77	         0.0	      207	        487	         505	       1028	  0.42505	1.3e-005	       243
   3	MA0495.2                  MAFF	DWWTTGCTGAGTCAGCAAWWW	2.5e-001	   1.8e-004	          -8.64	         0.0	      301	        479	         486	        678	  0.62839	7.4e-007	       239
   3	MA0014.3                  PAX5	RRGCGTGACCNN	9.7e-001	   6.8e-004	          -7.29	         0.0	      262	        488	         628	       1033	  0.53689	2.8e-006	       243
   3	MA0037.3                 GATA3	WGATAASR	7.1e-001	   5.0e-004	          -7.59	         0.0	      312	        492	         681	        966	  0.63415	2.1e-006	       245
   3	MA0099.3              FOS::JUN	ATGAGTCAYM	1.5e-001	   1.0e-004	          -9.18	         0.0	      166	        490	         381	        913	  0.33878	4.2e-007	       244
   3	MA1128.1            FOSL1::JUN	NKATGACTCATNN	1.7e-003	   1.2e-006	         -13.66	         0.0	      167	        487	         372	        848	  0.34292	4.8e-009	       243
   3	MA1130.1            FOSL2::JUN	NNRTGAGTCAYN	1.5e-001	   1.0e-004	          -9.17	         0.0	      164	        488	         346	        826	  0.33607	4.3e-007	       243
   3	MA1132.1             JUN::JUNB	KATGACKCAT	1.1e-001	   7.6e-005	          -9.48	         0.0	      300	        490	         764	       1116	  0.61224	3.1e-007	       244
   3	MA1134.1             FOS::JUNB	KATGASTCATHN	5.6e-003	   3.9e-006	         -12.44	         0.0	      228	        488	         381	        663	  0.46721	1.6e-008	       243
   3	MA1135.1            FOSB::JUNB	KRTGASTCAT	3.3e-002	   2.3e-005	         -10.67	         0.0	      226	        490	         400	        716	  0.46122	9.5e-008	       244
   3	MA1137.1           FOSL1::JUNB	NDRTGACTCATNN	3.0e-002	   2.1e-005	         -10.77	         0.0	      241	        487	         633	       1103	  0.49487	8.6e-008	       243
   3	MA1138.1           FOSL2::JUNB	KRTGASTCAT	9.9e-002	   7.0e-005	          -9.57	         0.0	      302	        490	         455	        640	  0.61633	2.9e-007	       244
   3	MA1141.1             FOS::JUND	NKATGAGTCATNN	2.3e-003	   1.6e-006	         -13.32	         0.0	      165	        487	         302	        679	  0.33881	6.7e-009	       243
   3	MA1142.1           FOSL1::JUND	NRTGACTCAT	1.2e-002	   8.8e-006	         -11.64	         0.0	      224	        490	         631	       1177	  0.45714	3.6e-008	       244
   3	MA1143.1    FOSL1::JUND(var.2)	RTGACGTMAY	6.5e0000	   4.6e-003	          -5.39	         0.0	      166	        490	         459	       1154	  0.33878	1.9e-005	       244
   3	MA1144.1           FOSL2::JUND	KATGACTCAT	1.0e0000	   7.3e-004	          -7.22	         0.0	      302	        490	         496	        710	  0.61633	3.0e-006	       244
   3	MA1153.1                 Smad4	TGTCTRGM	6.0e0000	   4.3e-003	          -5.46	         0.0	      426	        492	         390	        419	  0.86585	1.7e-005	       245
   3	MA1186.1             At1g49010	HWWAWYCTTATCYWH	5.2e-003	   3.7e-006	         -12.51	         0.0	      151	        485	         336	        833	  0.31134	1.5e-008	       242
   3	MA1188.1             At3g11280	AWYCTTATCTTHWY	4.5e-001	   3.1e-004	          -8.06	         0.0	      328	        486	         617	        822	  0.67490	1.3e-006	       242
   3	MA1192.1             At5g58900	WDWRGATAAGRTTWD	1.0e-002	   7.3e-006	         -11.83	         0.0	       99	        485	         175	        584	  0.20412	3.0e-008	       242
   3	MA1197.1                CAMTA1	AAARCGCGTGDD	1.5e0000	   1.0e-003	          -6.87	         0.0	      346	        488	         190	        227	  0.70902	4.3e-006	       243
   3	MA1399.1             At5g08520	DWWDWRGATAAGR	3.4e-002	   2.4e-005	         -10.65	         0.0	      117	        487	         179	        523	  0.24025	9.7e-008	       243
##
# Detailed descriptions of columns in this file:
#
# db:	The name of the database (file name) that contains the motif.
# id:	A name for the motif that is unique in the motif database file.
# alt:	An alternate name of the motif that may be provided
#	in the motif database file.
# consensus:	A consensus sequence computed from the motif.
# E-value:	The expected number motifs that would have least one.
#	region as enriched for best matches to the motif as the reported region.
#	The E-value is the p-value multiplied by the number of motifs in the
#	input database(s).
# adj_p-value:	The probability that any tested region would be as enriched for
#	best matches to this motif as the reported region is.
#	By default the p-value is calculated by using the one-tailed binomial
#	test on the number of sequences with a match to the motif 
#	that have their best match in the reported region, corrected for
#	the number of regions and score thresholds tested.
#	The test assumes that the probability that the best match in a sequence
#	falls in the region is the region width divided by the
#	number of places a motif
#	can align in the sequence (sequence length minus motif width plus 1).
#	When CentriMo is run in discriminative mode with a negative
#	set of sequences, the p-value of a region is calculated
#	using the Fisher exact test on the 
#	enrichment of best matches in the positive sequences relative
#	to the negative sequences, corrected
#	for the number of regions and score thresholds tested.
#	The test assumes that the probability that the best match (if any)
#	falls into a given region
#	is the same for all positive and negative sequences.
# log_adj_p-value:	Log of adjusted p-value.
# bin_location:	Location of the center of the most enriched region.
# bin_width:	The width (in sequence positions) of the most enriched region.
#	A best match to the motif is counted as being in the region if the
#	center of the motif falls in the region.
# total_width:	The window maximal size which can be reached for this motif:
#		rounded(sequence length - motif length +1)/2
# sites_in_bin:	The number of (positive) sequences whose best match to the motif
#	falls in the reported region.
#	Note: This number may be less than the number of
#	(positive) sequences that have a best match in the region.
#	The reason for this is that a sequence may have many matches that score
#	equally best.
#	If n matches have the best score in a sequence, 1/n is added to the
#	appropriate bin for each match.
# total_sites:	The number of sequences containing a match to the motif
#	above the score threshold.
# p_success:	The probability of falling in the enriched window:
#		bin width / total width
# p-value:	The uncorrected p-value before it gets adjusted to the
#	number of multiple tests to give the adjusted p-value.
# mult_tests:	This is the number of multiple tests (n) done for this motif.
#	It was used to correct the original p-value of a region for
#	multiple tests using the formula:
#		p' = 1 - (1-p)^n where p is the uncorrected p-value.
#	The number of multiple tests is the number of regions
#	considered times the number of score thresholds considered.
#	It depends on the motif length, sequence length, and the type of
#	optimizations being done (central enrichment, local enrichment,
#	score optimization).
