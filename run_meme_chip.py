import operator
import os

from functools import reduce
from glob import glob
from subprocess import Popen, PIPE


jaspar_db = os.path.expanduser('~/meme/db/motif_databases/JASPAR/JASPAR2018_CORE_non-redundant.meme')
hocomoco_db = os.path.expanduser('~/meme/db/motif_databases/HUMAN/HOCOMOCOv11_full_HUMAN_mono_meme_format.meme')


args = {
    'order': '1',
    'meme-mod': 'zoops',
    'meme-minw': '5',
    'meme-maxw': '30',
    'meme-nmotifs': '7',
    'dreme-e': '0.05',
    'centrimo-score': '5.0',
    'centrimo-ethresh': '10.0',
    'meme-p': '8',    # number of cores to run meme-chip on
}


def get_args(filename, db, dbtype):
    oc = './results/%s/%s/' % (os.path.basename(filename), dbtype)

    args.update({'oc': oc, 'db': db})
    return reduce(operator.concat, [('-%s' % key, value) for key,value in args.items()])


def run_meme_chip(args, filename):
    phandle = Popen([os.path.expanduser('~/meme/bin/meme-chip')] + list(args) + [filename])
    phandle.communicate()


def run_all_meme_chip(verbose=True):
    for filename in glob('./data/product/*.fa'):
        if verbose:
            print('Running MEME-ChIP against JASPAR CORE 2018 (non-redundant) on file: %s' % filename)

        run_meme_chip(
            get_args(filename=filename, db=jaspar_db, dbtype='jaspar'),
            filename,
        )

        if verbose:
            print('Running MEME-ChIP against HOCOMOCO v11 (FULL) on file: %s' % filename)

        run_meme_chip(
            get_args(filename=filename, db=hocomoco_db, dbtype='hocomoco'),
            filename,
        )

