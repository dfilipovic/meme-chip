import operator
import os
import numpy
import pandas

import seaborn as sns

from Bio import SeqIO
from glob import glob

from matplotlib import pyplot as plt


C_LEN = 501
DATAFILE_ALLPEAKS = './data/AHRpeaks_seqs.fa'


def seg_params(record):
    chrom, start, end  = [record.id.split(':')[0], *record.id.split(':')[1].split('-')]

    return chrom, int(start), int(end)


def midpoint(start, end):
    # returns a midpoint position, given a start and an end position
    #   assumes start is the position of the nucleotide preceeding the first nucleotide in the given sequence
    return int((end-start+1)/2)+start


# load records in a file
# replace records with chromosome, midpoint
# sort records by chromosome, midpoint
# get distributions

def cohesin_histogram_data(filename):
    records = list(SeqIO.parse(filename, 'fasta'))
    records = [seg_params(record) for record in records]
    records = [(record[0], midpoint(record[1], record[2])) for record in records]
    records = numpy.array(sorted(records, key=operator.itemgetter(0, 1)))
    chrom = pandas.Series(data=records[:,0], dtype="category")
    position = pandas.Series(data=records[:,1], dtype="int")
    records = pandas.DataFrame({'chrom': chrom, 'position': position})

    chroms = []
    distances = []
    for chrom in records.chrom.cat.categories:
        chrom_positions = records[records['chrom'] == chrom]['position']
        chrom_distances = [y - x for x, y in zip(chrom_positions[:-1], chrom_positions[1:])]

        chroms += [chrom]* len(chrom_distances)
        distances += chrom_distances

    return pandas.DataFrame({'chrom': chroms, 'distance': distances})


def plot(records, title, plt_type="violin", width=20, height=10, dpi=320, output_file=None, all_chroms=True):
    if plt_type == "violin":
        plt_f = sns.violinplot
    else:
        plt_f = sns.boxplot

    if all_chroms:
        ax = plt_f(x="chrom", y="distance", data=records)
    else:
        ax = plt_f(data=records.distance)
    ax.set_title(title)
    ax.plot()

    if output_file is not None:
        fig = plt.gcf()
        fig.set_size_inches(20, 11)
        fig.savefig(output_file, dpi=320)
        plt.clf()
    else:
        plt.show()

def central_region(record, c_len=C_LEN):
    # given a fasta record, returns the c_len centrally located nucleotides
    n = len(record)
    n2 = int(n/2)

    if n <= c_len:
        return record
    else:
        c_len2 = int(c_len/2)
        return record[n2-c_len2:n2+c_len2+1]


def find(records, chrom, start, end):
    # given a set of fasta records finds the one for a given
    #   chrom - chromosome
    #   start - start position
    #   end   - end position
    for record in records:
        if record.chrom == chrom and record.start == start and record.end == end:
            return record

    raise ValueError('Segment %s:%d-%d not found' % (chrom, start, end))


def subsample(records, bedfile):
    output_records = []
    for index, row in bedfile.iterrows():
        record = find(records, chrom=row[0], start=row[1], end=row[2])
        output_records.append(record)

    return output_records


def subsample_and_save(records, bedfile, outfilename):
    output_records = subsample(records, bedfile)

    if len(bedfile) != len(output_records):
        raise ValueError('Subsampling failed')

    SeqIO.write(output_records, outfilename, 'fasta')

    return output_records


def subsample_all(records, verbose=True):
    bedfilenames = glob('./data/*.bed')

    consumed_record_ids = {} 
    for bedfilename in bedfilenames:
        bedfile = pandas.read_csv(bedfilename, delimiter='\t')
        outfilename = os.path.join('./data/product/', os.path.basename(bedfilename) + '.fa')

        output_records = subsample_and_save(records, bedfile, outfilename)

        if verbose:
            print("Created file: '%s' with %d records" % (outfilename, len(output_records)))

        for record in output_records:
            if not bedfilename in consumed_record_ids:
                consumed_record_ids[bedfilename] = set()
            consumed_record_ids[bedfilename].add(record.id)
    return consumed_record_ids


class RecordSet(object):
    def __init__(self, records):
        self.records = records
        self.ids = [x.id for x in records]

    def has_record(self, record):
        return record.id in self.ids


def split_file(infilename, splitfilename1, splitfilename2, outfilename1, outfilename2, verbose=True):
    in_records = list(SeqIO.parse(infilename, 'fasta'))
    split_records1 = RecordSet(records=list(SeqIO.parse(splitfilename1, 'fasta')))
    split_records2 = RecordSet(records=list(SeqIO.parse(splitfilename2, 'fasta')))

    out_records1 = []
    out_records2 = []
    for record in in_records:
        if split_records1.has_record(record):
            out_records1.append(record)
        elif split_records2.has_record(record):
            out_records2.append(record)
        else:
            raise ValueError('Neither %s, nor %s contain record: %s' % (splitfilename1, splitfilename2, record.id))

    SeqIO.write(out_records1, outfilename1, 'fasta')
    SeqIO.write(out_records2, outfilename2, 'fasta')

    if verbose:
        print('Created file: "%s" with %d records' % (outfilename1, len(out_records1)))
        print('Created file: "%s" with %d records' % (outfilename2, len(out_records2)))



def subsample_all_summary(consumed_record_ids):
    # compare each set against all the other sets
    for bedfilename in consumed_record_ids:
        print("\n\nFile %s:" % bedfilename)
        cmp1 = consumed_record_ids[bedfilename]
        print("   Number of sequences: %d" % len(cmp1))
 
        for compare_bedfilename in consumed_record_ids:
            cmp2 = consumed_record_ids[compare_bedfilename]

            if compare_bedfilename != bedfilename:
                print("    Number of sequences also appearing in %s: %d" % (compare_bedfilename, len(cmp1.intersection(cmp2))))
    

def subsample_minumum_length(records, min_length=199):
    return [central_region(x, min_length) for x in records if len(x.seq) >= min_length]


def subsample_by_length(filename_pattern, verbose=True):
    filenames = glob(filename_pattern)
    record_lists = [list(SeqIO.parse(x, 'fasta')) for x in filenames]
    for max_length in [199, 299, 399, 499]:
        for index, records in enumerate(record_lists):
            filename = filenames[index]
            subsampled_records = subsample_minumum_length(records, max_length)

            if any(len(record) != max_length for record in subsampled_records):
                raise ValueError('Subsampling by length failed in file: %s for length: %d' % (filename, minlength))


            outfilename = '%s-%d.fa' % (os.path.splitext(filename)[0], max_length)
            SeqIO.write(subsampled_records, outfilename, 'fasta')
            
            if verbose:
                print('Created file: "%s" with %d records' % (outfilename, len(subsampled_records)))



def load_and_annotate(filename):
    # load all AhR peak sequence records
    records = list(SeqIO.parse(filename, 'fasta'))

    # annotate records with chromosome id, start position and end position
    for record in records:
        record.chrom, record.start, record.end = seg_params(record)
    return records
