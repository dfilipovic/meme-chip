MEME-ChIP analysis
------

Description of data files
=====

    File ``data/AHRpeaks_seqs.fa`` is a fasta file containing all sequences found under AhR peaks.

    Files ``data/*.bed`` are bed files containing the position of all sequences found under AhR peaks that -

        ``AHRpeaks_noAHREs.bed`` - with no AhREs

        ``AHRpeaks_oneAHRE.bed`` - with a single AhRE

        ``AHRpeaks_withAHREs.bed`` - with multiple AhREs

        ``AHRpeaks_not-in-DHSs.bed`` - with or without AhRE, but in closed chromatin

        ``AHRpeaks_CTCFpeaks_RAD21-coh-peaks_overlap.bed`` - coinciding with cohesin (RAD21) and CTCF

            - NOTE: original file contains non-unique entries

        ``AHRpeaks_RAD21-coh-peaks_overlap.bed`` - coinciding with cohesin (RAD21)

            - NOTE: original file contains non-unique entries

    File ``data/chromosome_lengths.csv`` created from data available at: https://www.ncbi.nlm.nih.gov/grc/human/data




Installation
====

Running in a virtual enviroment
~~~~~

To install all the requirements in a virtual enviroment first create a python3 virtual enviroment

    ``virtualenv -p python3 venv``

Activate the virtual environment

    ``. venv/bin/activate``


Update pip

    ``pip install pip --upgrade``


Install the requirements

    ``pip install -r requirements.txt``


Install the development requirements (ipython, jupyter, ipykernel)

    ``pip install -r requirements-dev.txt``


Create an ipython kernel for jupyter (to run notebooks within a virtual environment)

    ``ipython kernel install --user --name=meme-chip``


Run jupyter notebook, and open the appropriate notebook

    ``jupyter notebook``


Change kernel to meme-chip (Menu > Kernel > Change kernel > meme-chip



Requirements
=====


Sequence length
~~~~~~

According to the protocol publication MEME-ChIP requires (strongly recommends) the input data to be all the same length. In practice the online version seems to work fine with different length sequences.


Our data (``AHRpeaks_seqs.fa`` -- all AhR peaks) has:

    1 sequence that is shorter than 200

    15 sequences that are shorter than 300

    197 sequences that are shorter than 400

    659 sequences that are shorter than 500



Installing MEME-ChIP
=========

MEME-ChIP is the tool that performs the motif analysis and you have to install it for any of the code here to work.

Installation instructions for MEME-ChIP can be found on: http://meme-suite.org/doc/install.html?man_type=web

    *NOTE*: When installing MEME-ChIP you might have to install JSON package for perl:

        ``cpan JSON``




Web based analysis
=====

``AHRpeaks_seqs.fa``
    Description: All AhR peaks

    number of sequences: *2594*

    web based MEME-ChIP: http://meme-suite.org/opal-jobs/appMEMECHIP_4.12.01526937609696-1764388551/meme-chip.html


``AHRpeaks_noAHRE.bed.fa``
    Description: AhR peaks in regions not containing a single AhRE

    number of sequences: *1228*

    web based MEME-ChIP: http://meme-suite.org/opal-jobs/appMEMECHIP_4.12.01527020198776-1041492555/meme-chip.html 
    
    
``AHRpeaks_oneAHRE.bed.fa``
    Description: AhR peaks in regions containing exactly one AhRE

    number of sequences: *843*

    web based MEME-ChIP: http://meme-suite.org/opal-jobs/appMEMECHIP_4.12.01527027345654-1383657520/meme-chip.html
    

``AHRpeaks_withAHREs.bed``
    Description: AhR peaks in regions containing one or more AhRE

    number of sequences: *1364*

    Note: contains all sequences contained in ``AHRpeaks_oneAHRE.bed.fa`` (plus more)

    web based MEME-ChIP: http://meme-suite.org/opal-jobs/appMEMECHIP_4.12.01527027915385-127450498/meme-chip.html


``AHRpeaks_not-in-DHSs.bed.fa``
    Description: AhR peaks in regions of close chromatin

    number of sequences: *61*

    web based MEME-ChIP: http://meme-suite.org/opal-jobs/appMEMECHIP_4.12.01527026295618158656017/meme-chip.html
